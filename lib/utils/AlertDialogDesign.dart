import 'package:flutter/material.dart';

class AlertDialogDesign {

  static Widget goodAlertDialog(BuildContext context, String message,
      String title) {
    return Container(
      alignment: Alignment.center,
      child: AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0))),
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          RaisedButton(
            padding: EdgeInsets.all(15),
            color: Color(0xFF21f3e7),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30)),
            child: Text("Ok",
                style: TextStyle(color: Colors.white, fontSize: 20)),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
        elevation: 24,
      ),
    );
  }

  static Widget badAlertDialog(BuildContext context, String message,
      String title) {
    return Container(
      alignment: Alignment.center,
      child: AlertDialog(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0))),
        title: Text(title),
        content: Text(message),
        actions: <Widget>[
          RaisedButton(
            padding: EdgeInsets.all(15),
            color: Colors.red,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30)),
            child: Text("Ok",
                style: TextStyle(color: Colors.white, fontSize: 20)),
            onPressed: () => Navigator.of(context).pop(),
          ),
        ],
        elevation: 24,
      ),
    );
  }
}