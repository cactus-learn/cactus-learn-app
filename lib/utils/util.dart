import 'package:shared_preferences/shared_preferences.dart';

Future<String> getCurrentUserId()async{
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String id = sharedPreferences.get("id");
  return id;
}

Future<String> getCurrentUserToken()async{
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  String token = sharedPreferences.get("token");
  return token;
}
