import 'package:cactus/view/OnstartPage.dart';
import 'package:cactus/view/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.all(24),
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(
                  
                    image: AssetImage('assets/little.png'))),
          ),

          ListTile(
              leading: Icon(Icons.supervised_user_circle_sharp,color: Colors.green),
              title: Text('Profil'),
              onTap: () => {
                Navigator.of(context).push(MaterialPageRoute(
                    builder:(BuildContext context)=>ProfilePages())
                )}),

          ListTile(
              leading: Icon(Icons.logout,color: Colors.green),
              title: Text('Deconnexion '),
              onTap: () => {

                Navigator.of(context).push(MaterialPageRoute(
                    builder:(BuildContext context)=>OnstartPage())
                )})


        ]));



  }
}