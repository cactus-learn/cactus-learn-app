import 'package:flutter/cupertino.dart';

class ProgressBar extends StatelessWidget {
  const ProgressBar({
    Key key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 35,
      decoration: BoxDecoration(
        border: Border.all(color: Color(0xFF49B356), width: 3),
        borderRadius: BorderRadius.circular(50),
      ),
    );
  }
}
