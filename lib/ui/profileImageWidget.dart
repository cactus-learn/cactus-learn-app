import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileImageWidget extends StatelessWidget {
  final String image ="assets/little.png";

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40.0,
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(50.0),
          bottomRight: Radius.circular(50.0),
          topLeft: Radius.circular(50.0),
        ),
        image: DecorationImage(
          image: AssetImage(
            image,
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
