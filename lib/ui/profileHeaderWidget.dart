import 'package:flutter/cupertino.dart';

class ProfileHeaderWidget extends StatelessWidget {
  final String title;
  final String desc;
  final String secondTitle;
  final String secondDesc;

  const ProfileHeaderWidget({Key key, this.title, this.desc, this.secondTitle, this.secondDesc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        SizedBox(
          height: 10.0,
        ),

      ],
    );
  }
}
