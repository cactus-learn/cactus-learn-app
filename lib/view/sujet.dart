import 'package:cactus/ui/NavDrawer.dart';
import 'package:cactus/ui/profileImageWidget.dart';
import 'package:cactus/view/publication.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file.dart';


class Sujet extends StatefulWidget {
  final String  lessonId;
  Sujet(this.lessonId);


  @override
  _SujetState createState() => _SujetState();
}

class _SujetState extends State<Sujet> {




  @override  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          drawer: NavDrawer(),
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () => Navigator.of(context).pop(),
            ),
            elevation: 0.0,
            backgroundColor:Color(0xFF4DD080),
            //App Bar Back Button
            title: Text("Mes Sujet ",
              style: TextStyle(
                color: Colors.white,
                fontSize: 17,
                fontFamily: "Montserrat",
              ),
            ),
            bottom: TabBar(
              tabs: [
                Tab(text: "Publication", icon: Icon(Icons.public_sharp)),
                Tab(text: "File",icon: Icon(Icons.file_copy_rounded)),
              ],
            ),
            actions: <Widget>[
              // Profile Image Widget
              ProfileImageWidget(),
            ],
          ),
          floatingActionButtonLocation :FloatingActionButtonLocation.centerFloat,
          body: TabBarView(
            children: [
              Publication(this.widget.lessonId),
              MyFile(lessonId: this.widget.lessonId),
            ],
          ),
        ),
      ),
    );
  }
}



