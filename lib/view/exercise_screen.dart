
import 'package:cactus/model/exercicesMdel.dart';
import 'package:cactus/model/questionModel.dart';
import 'package:cactus/service/exercicesService.dart';
import 'package:cactus/view/congratepage.dart';
import 'package:flutter/material.dart';

class ExerciseScreen extends StatefulWidget{

  ExercicesModel exercice;
  ExerciseScreen({this.exercice});

  @override
  _ExerciseScreen createState() => _ExerciseScreen();
}

class _ExerciseScreen extends State<ExerciseScreen>{

  int selectRadio = 0;

  updateQuestion(QuestionModel question, int index){
    setState(() {
      widget.exercice.questions[index] = question;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor:Color(0xFF4DD080),
        //App Bar Back Button
        title: Text("Exercice : "+widget.exercice.title,
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            for(var i = 0; i < widget.exercice.questions.length; i++) QuestionHandling(questionModel: widget.exercice.questions[i], notifyParent: updateQuestion, index: i),
            ElevatedButton(
              onPressed: () {
                postUserResponse(widget.exercice);
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (BuildContext context) =>
                    CongratsPage()),
                        (Route<dynamic> route) => true);
              },
              child: Text('Envoyer les réponses'),
            )
          ],
        ),)
    );
  }
}

class QuestionHandling extends StatefulWidget{
  QuestionModel questionModel;
  int index;

  final Function(QuestionModel question, int index) notifyParent;

  QuestionHandling({this.questionModel, @required this.notifyParent, @required this.index});
  @override
  _QuestionHandling createState() => _QuestionHandling(question: questionModel);

}

class _QuestionHandling extends State<QuestionHandling>{

  QuestionModel question;
  _QuestionHandling({this.question});

  void updateResponses(List<ResponsesModel> responses){
    setState(() {
      question.responses = responses;
      widget.notifyParent(question, widget.index);
    });

  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Text(question.text,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 40,
            ),),
          margin: new EdgeInsets.symmetric(vertical: 20.0),),
        Divider(),
        Column(
          children: <Widget>[
            new ResponseHandling(responses: question.responsesModel, questionType: question.questionType, notifyParent: updateResponses)
          ],
        ),
      ],
    );
  }

}

class ResponseHandling extends StatefulWidget{

  List<ResponsesModel> responses;
  String questionType;
  final Function(List<ResponsesModel> responses) notifyParent;
  ResponseHandling({this.responses, this.questionType, @required this.notifyParent});

  @override
  _ResponseHandling createState() => new _ResponseHandling(responses: responses, questionType: questionType);

}

class _ResponseHandling extends State<ResponseHandling>{

  List<ResponsesModel> responses;
  String questionType;
  int groupId = -1;
  _ResponseHandling({this.responses, this.questionType});


  Widget generateResponseList(){
    if(questionType.toLowerCase() == "qcm"){
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          for(var i = 0; i < responses.length; i++) generateCheckBox(responses[i].text, i)
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          for(var i = 0; i < responses.length; i++) generateRadioButton(responses[i].text, i)
        ],
      );
    }
  }

  Widget generateRadioButton(val, i){
    return RadioListTile(
        contentPadding: new EdgeInsets.all(15),
        value: i,
        groupValue: groupId,
        title: Text(val),
        onChanged: (value) {
          setState(() {
            groupId = value;
            for(var j=0; j<responses.length; j++) {
              responses[j].isUserResponse = false;
            }
            responses[i].isUserResponse = true;
            widget.notifyParent(responses);
          });
        });
  }

  Widget generateCheckBox(val, i){
    return CheckboxListTile(
      contentPadding: new EdgeInsets.all(15),
      title: Text(val),
      value: responses[i].isUserResponse,
      onChanged: (value) {
        setState(() {
          responses[i].isUserResponse = value;
          widget.notifyParent(responses);
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return generateResponseList();
  }

}