import 'dart:async';
import 'dart:developer';
import 'package:cactus/model/FileModel.dart';
import 'package:cactus/view/VideoView.dart';
import 'package:cactus/view/ControllerOverlayVideo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';


class ListOfFile extends StatefulWidget {

  FileModel file;
  String type;
  ListOfFile({this.file,this.type});

  @override
  _ListOfFileState createState() => _ListOfFileState();
}

class _ListOfFileState extends State<ListOfFile> {
  FileModel file;
  String type;
  StreamController streamController = StreamController();
  VideoPlayerController videoPlayerController;
  VideoPlayerController _controller;
  String urlVideo;

  void fetchData(){
    file = widget.file;
    type = widget.type;
    streamController.add(file);
  }


  getWidgetFromOfTypeFile(String type, String url){
    log("TYPE : "+type);
    switch(type){
      case "video":  return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(image : AssetImage("assets/lilitelecac.jpg"),width: 200, height: 200,),
            TextButton(onPressed :()  {
            Navigator.pushReplacement(context,  MaterialPageRoute(
            builder: (_) => VideoView(
            url : url,
            ),
            ));
            }, child: Text("Lancer la video", style: TextStyle(fontSize: 20),)),
            Center(
              child: IconButton(onPressed: () {
                Navigator.pushReplacement(context,  MaterialPageRoute(
                  builder: (_) => VideoView(
                    url : url,

                  ),
                ));
              },icon: Icon(Icons.play_arrow, size: 50,)),
            )
          ],),
        ),
      );
      case "image": return showImage(url);
    }
  }

  showImage(String url){
    return FadeInImage(placeholder: AssetImage('assets/lilitelecac.jpg'), image: NetworkImage(url));
  }

  @override
  Widget build(BuildContext context) {
    fetchData();
    return Scaffold(
      appBar: AppBar(),
      body: StreamBuilder(
        stream: streamController.stream,
        builder: (BuildContext context,AsyncSnapshot snapshot) {
          if(snapshot.hasData){
            return getWidgetFromOfTypeFile(type, file.src);
          }
          else{
            return   Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "il y'a pas de fichier disponible pour le moment ",
                    textAlign: TextAlign.center,
                  ),
                  Divider(),
                  Image.asset('assets/tenor.gif'),
                  Divider(),
                  FlatButton.icon(
                    color: Colors.green,
                    icon: Icon(Icons.check),
                    label: Text(' fichier non mis en ligne'),
                  )
                ],
              ),
            );
          }
        },
      ),
    );
  }


  setDataVideo(String url){
    _controller = VideoPlayerController.network(
      url,
      videoPlayerOptions: VideoPlayerOptions(mixWithOthers: true),
    );
    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize();
  }


  showVideoView(String url){
    setDataVideo(url);
     return SingleChildScrollView(
                  child: Column(
                    children: <Widget>[
                      Container(padding: const EdgeInsets.only(top: 20.0)),
                      const Text('Video'),
                      Container(
                        padding: const EdgeInsets.all(20),
                        child: AspectRatio(
                          aspectRatio: _controller.value.aspectRatio,
                          child: Stack(
                            alignment: Alignment.bottomCenter,
                            children: <Widget>[
                              VideoPlayer(_controller),
                              ClosedCaption(text: _controller.value.caption.text),
                              ControlsOverlay(controller: _controller),
                              VideoProgressIndicator(_controller, allowScrubbing: true),
                            ],
                          ),
                        ),
                      ),
                      ButtonBar(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          TextButton(onPressed: () { dispose(); Navigator.pop(context); },
                            child: Text("Fermer"),)
                        ],)
                    ],
                  ),
                );
  }


 



}

