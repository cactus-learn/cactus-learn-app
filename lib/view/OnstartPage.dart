import 'package:cactus/ui/style.dart';
import 'package:cactus/view/register.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'login.dart';

class OnstartPage extends StatefulWidget {
  @override
  _OnstartPageState createState() => _OnstartPageState();
}
class _OnstartPageState extends State<OnstartPage> {


  final int _numPages = 3;
  final PageController _pageController = PageController(initialPage: 0);
  int _currentPage = 0;

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0; i < _numPages; i++) {
      list.add(i == _currentPage ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      height: 8.0,
      width: isActive ? 24.0 : 16.0,
      decoration: BoxDecoration(
        color: isActive ? Colors.white : Color(0xFF66FF01),
        borderRadius: BorderRadius.all(Radius.circular(12)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              stops: [0.1, 0.4, 0.7, 0.9],
              colors: [
                Color(0xFF56E78E),
                Color(0xFF4DD080),
                Color(0xFF009252),
                Color(0xFF006B3C),

              ],
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    onPressed:() {
                      Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (context)=> Loginpage()
                          )
                      );
                    },

                    //onPressed: () => print('Skip'),
                    child: Text(
                      'Passer et se connecter',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 600.0,
                  child: PageView(
                    physics: ClampingScrollPhysics(),
                    controller: _pageController,
                    onPageChanged: (int page) {
                      setState(() {
                        _currentPage = page;
                      });
                    },
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(40.0),
                        child: ListView(
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage(
                                  'assets/onboarding.png',
                                ),
                                height: 330.0,
                                width: 330.0,
                              ),
                            ),
                            SizedBox(height: 30.0),
                            Text(
                              'Evaluez-vous',
                              style: style2,
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              'Découvrez votre niveau et définissez votre objectif',
                              style: style1,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(40.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage(
                                  'assets/onboarding2.png',
                                ),
                                height: 330.0,
                                width: 330.0,
                              ),
                            ),
                            SizedBox(height: 30.0),
                            Text(
                              'Renforcez vos compétences',
                              style: style1,
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              'Bâtissez des fondations solides pour progresser rapidement',
                              style: style2,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(40.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: Image(
                                image: AssetImage(
                                  'assets/onboarding3.png',
                                ),
                                height: 330.0,
                                width: 330.0,
                              ),
                            ),
                            SizedBox(height: 30.0),
                            Text(
                              'Réussissez vos formation',
                              style: style1,
                            ),
                            SizedBox(height: 15.0),
                            Text(
                              'Général, métier ou sectoriel, des parcours adaptés à votre projet',
                              style: style2,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _buildPageIndicator(),
                ),
                _currentPage != _numPages - 1
                    ? Expanded(
                  child: Align(
                    alignment: FractionalOffset.bottomRight,
                    child: FlatButton(
                      onPressed: () {
                        _pageController.nextPage(
                          duration: Duration(milliseconds: 500),
                          curve: Curves.ease,
                        );
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Text(
                            'Suivant',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 22.0,
                            ),
                          ),
                          SizedBox(width: 10.0),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                            size: 30.0,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
                    : Text(''),
              ],
            ),
          ),
        ),
      ),
      bottomSheet: _currentPage == _numPages - 1
          ? Container(
        height: 100.0,
        width: double.infinity,
        color: Colors.white,
        child: FlatButton(
          onPressed:() {
            Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context)=> Registerpage()
                )
            );
          },
          // onTap: () => print('Register '),
          child: Center(
            child: Padding(
              padding: EdgeInsets.only(bottom: 30.0),
              child: Text(
                'S\'inscrire',
                style: TextStyle(
                  color: Color(0xFF66FF01),
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ),
      )
          : Text(''),
    );
  }
}




