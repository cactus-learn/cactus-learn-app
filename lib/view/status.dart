import 'package:cactus/ui/NavDrawer.dart';
import 'package:cactus/ui/profileImageWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StatusPage extends StatefulWidget {
  @override
  _StatusPageState createState() => _StatusPageState();
}

class _StatusPageState extends State<StatusPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0.0,
        backgroundColor: Color(0xFF4DD080),
        //App Bar Back Button
        title: Text("Stutus",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          // Profile Image Widget
          ProfileImageWidget(),
        ],
      ),

    );
}}
