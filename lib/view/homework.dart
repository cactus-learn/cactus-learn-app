import 'package:cactus/ui/profileImageWidget.dart';
import 'package:cactus/view/groupOfEercices.dart';
import 'package:cactus/view/notes.dart';
import 'package:cactus/view/publication.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'file.dart';

class HomeWork extends StatefulWidget {
  @override
  _HomeWorkState createState() => _HomeWorkState();
}

class _HomeWorkState extends State<HomeWork> {
  @override  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor:Color(0xFF4DD080),
            //App Bar Back Button
            title: Text("Mes Devoirs ",
              style: TextStyle(
                color: Colors.white,
                fontSize: 17,
                fontFamily: "Montserrat",
              ),
            ),
            bottom: TabBar(
              tabs: [
                Tab(text: "Mes exercices", icon: Icon(Icons.group_work_outlined)),
                Tab(text: "mes notes",icon: Icon(Icons.golf_course_outlined)),
              ],
            ),
            actions: <Widget>[
              // Profile Image Widget
              ProfileImageWidget(),
            ],
          ),
          body: TabBarView(
            children: [
              GroupOfEercices(),
              Notes()
            ],
          ),
        ),
      ),
    );
  }


}
