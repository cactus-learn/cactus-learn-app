import 'package:cactus/view/homework.dart';
import 'package:cactus/view/teams.dart';
import 'package:flutter/material.dart';
import 'GroupListforForum.dart';
import 'login.dart';



class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}
class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    Teams(),
    HomeWork(),
    GroupListForum(),

  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex], // new
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        type: BottomNavigationBarType.fixed,// new
        currentIndex: _currentIndex, // new
        items: [
          new BottomNavigationBarItem(
            icon: Icon(Icons.group),
            label:('Equipes'),
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.file_copy),
              label: ('Devoirs')
          ),
          new BottomNavigationBarItem(
              icon: Icon(Icons.forum),
              label: ('Forum')
          ),
        ],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
