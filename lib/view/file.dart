
import 'dart:async';

import 'package:cactus/model/ChapterModel.dart';
import 'package:cactus/model/LessonModel.dart';
import 'package:cactus/service/lessonService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'VideoView.dart';
import 'listOfFile.dart';

class MyFile extends StatefulWidget {

  String lessonId;
  MyFile({this.lessonId});

  @override
  _MyFileState createState() => _MyFileState();
}

class _MyFileState extends State<MyFile> {
  LessonModel lessonModel;
  StreamController streamController = StreamController();
  final snackBar = SnackBar(content: Text('Fichier indisponible'));

  void fetchData(){
   getLessonById(widget.lessonId).then((value){
     lessonModel = value;
     streamController.add(lessonModel);
   });
  }

  getIconTypeFile(String type){
    switch(type){
      case "video": return Icons.video_label;
      case "image": return Icons.image;
      default : return Icons.remove;
    }
  }


  @override
  Widget build(BuildContext context) {
    fetchData();
    return Scaffold(
      body: StreamBuilder(
        stream: streamController.stream,
        builder: (BuildContext context,AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done){
            if(snapshot.hasError){
              return Text("Aucune donnéé disnponible");
            }
            if(snapshot.data == null){
              return Text("Aucune donnéé disnponible");
            }
          }
          if (snapshot.connectionState == ConnectionState.none){
            if(snapshot.hasError){
              return Text("Aucune donnéé disnponible");
            }
            if(snapshot.data == null){
              return Text("Aucune donnéé disnponible");
            }
          }
          if(snapshot.hasData){
            return Center(
              child: Column(
                children: <Widget>[
                  Padding(padding: EdgeInsets.all(5),
                  child: Text(lessonModel.title,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),) ,),
                  Padding(padding: EdgeInsets.all(50),
                  child:  generateListFile()
                  ),
                ],
              )
            );
          }
          else{
            return Center(child: CircularProgressIndicator(),
          );
          }
        },
      ),
    );
  }

  generateFileView(ChapterModel chapter) {
    if(chapter.fileModel != null || chapter.fileModel != []) {
      return Card(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: InkWell(
                      splashColor: Colors.lightGreen.withAlpha(30),
                      onTap: () =>
                      {
                        if(chapter.fileModel.type == "video" ||
                            chapter.fileModel.type == "image"){
                          Navigator.push(context, MaterialPageRoute(
                            builder: (_) =>
                                ListOfFile(
                                  file: chapter.fileModel,
                                  type: chapter.fileModel.type,
                                ),
                          ))
                        }
                        else
                          {
                            ScaffoldMessenger.of(context).showSnackBar(snackBar)
                          }
                      },
                      child:
                      ListTile(
                          title: Text(chapter.title)
                      ),
                    ),
                  ),

                  IconButton(onPressed: () {
                    if (chapter.fileModel.type == "video" ||
                        chapter.fileModel.type == "image") {
                      Navigator.push(context, MaterialPageRoute(
                        builder: (_) =>
                            getWidgetFromOfTypeFile( chapter.fileModel.type, chapter.fileModel.src),
                      ));
                    }
                    else {
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  }, icon: Icon(getIconTypeFile(chapter.fileModel.type)))
                ],
              )
          );
    }else {
      return SizedBox.shrink();
    }
  }



  generateListFile(){
    return ListView.builder(
        shrinkWrap: true,
        itemCount: lessonModel.chapterModels.length,
        itemBuilder: (context, index) {
          return generateFileView(lessonModel.chapterModels[index]);
        });
  }

  getWidgetFromOfTypeFile(String type, String url){
    switch(type){
      case "video":  return Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(image : AssetImage("assets/lilitelecac.jpg"),width: 200, height: 200,),
              TextButton(onPressed :()  {
                Navigator.pushReplacement(context,  MaterialPageRoute(
                  builder: (_) => VideoView(
                    url : url,
                  ),
                ));
              }, child: Text("Lancer la video", style: TextStyle(fontSize: 20),)),
              Center(
                child: IconButton(onPressed: () {
                  Navigator.pushReplacement(context,  MaterialPageRoute(
                    builder: (_) => VideoView(
                      url : url,

                    ),
                  ));
                },icon: Icon(Icons.play_arrow, size: 50,)),
              )
            ],),
        ),
      );
      case "image": return showImage(url);
    }
  }

  showImage(String url){
    return FadeInImage(placeholder: AssetImage('assets/lilitelecac.jpg'), image: NetworkImage(url));
  }
}
