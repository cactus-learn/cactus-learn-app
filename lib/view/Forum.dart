import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:cactus/model/ForumModel.dart';
import 'package:cactus/model/PostModel.dart';
import 'package:cactus/model/PostResponse.dart';
import 'package:cactus/model/userResponse.dart';
import 'package:cactus/service/convService.dart';
import 'package:cactus/service/forumService.dart';
import 'package:cactus/ui/NavDrawer.dart';
import 'package:cactus/ui/profileImageWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Forum extends StatefulWidget {

  static const routeName = '/forum';
  ForumModel arguments;


  @override
  ForumState createState() => ForumState();
}

class ForumState extends State<Forum> {
  List<PostModel> listPost = [];
  List<PostModel> listPostModelId = [];
  StreamController streamController = StreamController();
  var writePostMessageController = TextEditingController();
  ForumModel forumModel;
  String postTowrite = "";
  String currentUserId = "";


  void fetchData() async{
    forumModel = ModalRoute.of(context).settings.arguments;
    Future<ForumModel> model = getForumById(forumModel.id);
    model.then((value) => {listPost = value.postModels, log(value.postModels.length.toString()),streamController.add(listPost)});
  }
  clearWriteMessageLabel(){
    writePostMessageController.clear();
  }

  sendPost(String content)async{
    createPost(content, forumModel.id).then((PostResponse value) => {
      if(value != null) {
        log("OK"),
      clearWriteMessageLabel(),
        setState((){ listPost.add(value.post);}),
    }
    });
  }

  void likePost(String postId){

  }

  Widget builPost() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      height: 110.0,
      color: Colors.white,
      child: Row(
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.photo),
            iconSize: 25.0,
            color: Colors.green,
            onPressed: () {},
          ),
          Expanded(
            child: TextField(

              controller: writePostMessageController,
              autocorrect: true,
              textCapitalization: TextCapitalization.sentences,
              onChanged: (value) {postTowrite = value;},
              decoration: InputDecoration.collapsed(
                hintText: 'Ecrire un post',
              ),
            ),
          ),
          IconButton(

            icon: Icon(Icons.send),
            iconSize: 25.0,
            color: Colors.green,
            onPressed: () async => {sendPost(postTowrite),
            },
          ),
        ],
      ),
    );
  }

  bool isThumbPressed(List<UserResponse> thumbs){
    for(UserResponse thumb in thumbs){
      if (thumb.id == currentUserId){
        return true;
      }
    }
    return false;

  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    listPost.clear();
    fetchData();
    getCurrentUserId().then((value) => currentUserId = value);
    return Scaffold(
      drawer: NavDrawer(),
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          elevation: 0.0,
          backgroundColor: Color(0xFF4DD080),
          //App Bar Back Button
          title: Text("Mon forum ",
            style: TextStyle(
              color: Colors.white,
              fontSize: 17,
              fontFamily: "Montserrat",
            ),
          ),
          centerTitle: true,
          actions: <Widget>[
            // Profile Image Widget
            ProfileImageWidget(),

          ],
        ),
      body: Column(
        children:<Widget> [
          Expanded(
          child:  Container(
            child: StreamBuilder(
              stream: streamController.stream,
              builder: ( BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.hasData){
                  return Container(
                    child: ListView.builder(
                      itemCount: listPost.length,
                      itemBuilder: (context, index) {
                        return Card(
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text(listPost[index].content),
                                //Todo: Changer en creator name
                                subtitle: Text(listPost[index].creatorId),
                              ),
                              ButtonBar(
                                alignment: MainAxisAlignment.end,
                                children: [
                                  IconButton(onPressed:() => {ratePost(listPost[index].id, true).then((value) => setState((){}))  },
                                      icon: Icon(Icons.thumb_up, color: isThumbPressed(listPost[index].thumb_up)? Colors.green : Colors.grey)),
                                  IconButton(onPressed:() => {ratePost(listPost[index].id, false).then((value) => setState((){}))  },
                                      icon: Icon(Icons.thumb_down,  color: isThumbPressed(listPost[index].thumb_down)? Colors.red : Colors.grey)),
                                ],
                              )
                            ],


                          ),
                        );
                      },
                    ),
                  );
                }
                else{
                  return Center(
                      child :
                      CircularProgressIndicator()
                  );
                }
              },
            ),
          ),
        ),
        builPost()],
      )
    );
  }

}
