import 'dart:async';

import 'package:cactus/service/CourService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Publication extends StatefulWidget {

  final String  lessonId;
  Publication(this.lessonId);


  @override
  _PublicationState createState() => _PublicationState();
}

class _PublicationState extends State<Publication> {
  StreamController streamController = StreamController();

  String lessonId;
  List _listLesson;
  var isLoading = true;


  @override
  void initState() {
    super.initState();
    lessonId = widget.lessonId;
    getLesson(lessonId).then((result) {
      setState(() {
        _listLesson = result;
      });
    }).whenComplete((){
      setState(() {
        isLoading = false;
      });
    });
  }


  Future<void> refreshOrga() async {
    var result = await  getCourOfOrga(lessonId);
    setState(() {
      _listLesson = result;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StreamBuilder(
            stream: streamController.stream,
          builder: (context, snapshot) {
            return ListView(
              padding: const EdgeInsets.all(8),
              children: [Stack(
                  children: [
                    Container(
                      height: MediaQuery
                          .of(context)
                          .size
                          .height - 82.0,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      color: Colors.transparent,
                    ),
                    Positioned(
                        top: 10.0,
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                          ),
                          height:  MediaQuery.of(context).size.height - 110.0,
                          width:  MediaQuery.of(context).size.width,
                          child: _listLesson != null ?
                          new RefreshIndicator(
                              onRefresh: refreshOrga,
                              child :  ListView.builder(
                                  padding: const EdgeInsets.all(8),
                                  itemCount: _listLesson.length,
                                  itemBuilder: (BuildContext context, int index) {
                                    return Container(
                                        height: 100,
                                        decoration: BoxDecoration(
                                          border: Border(
                                            bottom: BorderSide(
                                                width: 1.0, color: Colors.grey[300]),
                                          ),
                                        ),
                                        child: Padding(
                                            padding: EdgeInsets.all(16),
                                            child: Wrap(
                                              direction: Axis.horizontal,
                                              children: <Widget>[
                                                Center(
                                                  child: ListTile(
                                                    contentPadding: EdgeInsets.all(5.0),
                                                    leading: SizedBox(
                                                      height: 100.0,
                                                      width: 100.0, // fixed width and height
                                                    ),

                                                    title: Text('${_listLesson[index]["title"]}',
                                                      style: TextStyle( fontSize: 18),
                                                    ),
                                                  ),
                                                ),
                                                Text(_listLesson[index]["content"])
                                              ],
                                            )
                                        )

                                    );
                                  }
                              )

                          ):
                          Center(child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "il y'a pas de fichier disponible pour le moment ",
                                textAlign: TextAlign.center,
                              ),
                              Divider(),
                              Image.asset('assets/tenor.gif'),
                              Divider(),
                              FlatButton.icon(
                                color: Colors.green,
                                icon: Icon(Icons.check),
                                label: Text(' fichier non mis en ligne'),
                              )
                            ],
                          ),
                          ),

                        )

                    )
                  ]

              )


              ],

            );
          }
        )
    );
  }
}
