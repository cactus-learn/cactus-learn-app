import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';


class VideoView extends StatefulWidget {
  String url;


  VideoView({this.url});

  @override
  _VideoView createState() => _VideoView(url: url);
}

class _VideoView extends State<VideoView> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  String url;
  bool _isFullScreen = true;

  _VideoView({this.url});



  @override
  void initState() {
    _controller = VideoPlayerController.network(
      url,
    );

    _initializeVideoPlayerFuture = _controller.initialize();
    _controller.setLooping(true);

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Butterfly Video'),
      ),
      body: FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Center(
              child: RotatedBox(quarterTurns: _isFullScreen?0:1,
                child: AspectRatio(
                  aspectRatio: _controller.value.aspectRatio,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      VideoPlayer(_controller),
                      ClosedCaption(text: _controller.value.caption.text),
                      AnimatedSwitcher(
                        duration: Duration(milliseconds: 50),
                        reverseDuration: Duration(milliseconds: 200),
                        child: _controller.value.isPlaying
                            ? SizedBox.shrink()
                            : Container(
                          color: Colors.black26,
                          child: Center(
                            child: Icon(
                              Icons.play_arrow,
                              color: Colors.white,
                              size: 100.0,
                            ),
                          ),
                        ),
                      ),
                      VideoProgressIndicator(_controller, allowScrubbing: true),
                      GestureDetector(
                        onTap: () {
                          playPause();
                        },
                      ),
                    ],
                  ),
                ),),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          playPause();
        },
        child: Icon(
          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
        ),
      ),
    );
  }

  void playPause() {
    setState(() {
      if (_controller.value.isPlaying) {
        _controller.pause();
        _isFullScreen=!_isFullScreen;
      } else {
        _controller.play();
        _isFullScreen=!_isFullScreen;
      }
    });
  }
}


