import 'dart:async';
import 'package:cactus/model/exerciceGlobal.dart';
import 'package:cactus/service/exercicesService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'coursOfExercices.dart';



class GroupOfEercices extends StatefulWidget {
  @override
  _GroupOfEercicesState createState() => _GroupOfEercicesState();
}

class _GroupOfEercicesState extends State<GroupOfEercices> {
  List<ExerciceGlobal> myExercises =[] ;
  StreamController streamController = StreamController();
  List<ExerciceGlobal> listDataOrganisation;


  Widget groupOfExercisesLisGroup(){
    try{
      Future<List<ExerciceGlobal>> model = getListExercices();
      model.then((value) => {myExercises = value, streamController.add(value)});
    }catch(e){

    }
    return (
        Container(
          child: StreamBuilder(
              stream: streamController.stream,
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  return ListView.builder(
                    itemCount: myExercises.length,
                    itemBuilder: (context, index) {
                      return Card(
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).push(MaterialPageRoute
                            (builder: (BuildContext context) =>
                              CoursOfexercice(exercicesId : myExercises[index].groupeId, groupeName: myExercises[index].groupeName),
                          ));
                        },
                            child:ListTile(
                              title: Text(
                                  myExercises[index].groupeName
                              ),
                            ) ,
                          )
                      );
                    },
                  );

                }
                else{
                  return Center(
                      child :
                      CircularProgressIndicator()
                  );
                }
              }
          ),
        )
    );

  }
  void fetchData() async{
    Future<List<ExerciceGlobal>> model = getListExercices();
    model.then((value) => {myExercises = value, streamController.add(value)});
  }
  @override
  Widget build(BuildContext context) {
    myExercises.clear();
    return Scaffold(
      body: groupOfExercisesLisGroup(),

    );


  }

}