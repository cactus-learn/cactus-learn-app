import 'package:cactus/service/teamService.dart';
import 'package:cactus/ui/NavDrawer.dart';
import 'package:cactus/ui/profileImageWidget.dart';
import 'package:flutter/material.dart';

import 'courses.dart';


class Teams extends StatefulWidget {
  @override
  _TeamsState createState() => _TeamsState();
}

class _TeamsState extends State<Teams> {
  List _Orga;
  var isLoading = true;


  @override
  void initState() {
    super.initState();
    getOrgaOfuserIn().then((result) {
      print(result);
      setState(() {
        _Orga = result;
      });
    }).whenComplete(() {
      setState(() {
        isLoading = false;
      });
    });
  }


  Future<void> refreshOrga() async {
    var result = await getOrgaOfuserIn();
    setState(() {
      _Orga = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavDrawer(),
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Color(0xFF4DD080),
          //App Bar Back Button
          title: Text("Mes Groupes ",
            style: TextStyle(
              color: Colors.white,
              fontSize: 17,
              fontFamily: "Montserrat",
            ),
          ),
          centerTitle: true,
          actions: <Widget>[
            // Profile Image Widget
            ProfileImageWidget(),

          ],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: ListView(
          padding: const EdgeInsets.all(8),
          children: [Stack(
              children: [
                Container(height: MediaQuery.of(context).size.height - 82.0, width: MediaQuery.of(context).size.width, color: Colors.transparent,
                ),
                Positioned(
                    top: 10.0,
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        height:  MediaQuery.of(context).size.height - 110.0,
                        width:  MediaQuery.of(context).size.width,
                        child: _Orga != null ?
                        new RefreshIndicator(
                            onRefresh: refreshOrga,
                            child :  ListView.builder(
                                padding: const EdgeInsets.all(8),
                                itemCount: _Orga.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                                      height: 100,
                                      decoration: BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                              width: 1.0, color: Colors.grey[300]),
                                        ),
                                      ),
                                      child: (
                                          ListTile(
                                            contentPadding: EdgeInsets.all(16.0),
                                            leading: SizedBox(
                                                height: 100.0,
                                                width: 100.0, // fixed width and height
                                                child: Image.asset("assets/teams.png")
                                            ),
                                            trailing: Icon(Icons.arrow_forward_ios),
                                            title: Text('${_Orga[index]["name"]}',
                                              style: TextStyle( fontSize: 18),
                                            ),
                                            onTap: () {
                                              Navigator.of(context).push(MaterialPageRoute
                                                (builder: (BuildContext context) =>
                                                  Cours('${_Orga[index]["_id"]}'),
                                              ));
                                            },
                                          )
                                      )


                                  );


                                }


                            )

                        ):
                        Text("Vous ne faite parti d'aucune organiation ")

                    )

                )
              ]

          )


          ],

        )

    );
  }


}
