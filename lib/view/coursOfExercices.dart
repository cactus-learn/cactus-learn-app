import 'dart:async';

import 'package:cactus/model/exerciceGlobal.dart';
import 'package:cactus/model/exercicesMdel.dart';
import 'package:cactus/service/exercicesService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'exercise_screen.dart';

class CoursOfexercice extends StatefulWidget {

  final String  exercicesId;
  final String groupeName;
  CoursOfexercice({this.exercicesId, this.groupeName});



  static const routeName = '/test';
  List<ExerciceGlobal> arguments;
  @override
  _CoursOfexerciceState createState() => _CoursOfexerciceState();
}

class _CoursOfexerciceState extends State<CoursOfexercice> {
 ExerciceGlobal listExoGlobal ;
 List<ExercicesModel> exercices;
  StreamController streamController = StreamController();


  void fetchData() async{
    Future<ExerciceGlobal> model = getListExerciceByidGroup(widget.exercicesId);
    model.then((value) => {listExoGlobal = value,exercices=value.exercises, streamController.add(value)});

  }

      @override
      Widget build(BuildContext context) {
        fetchData();

        return Scaffold(
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor:Color(0xFF4DD080),
              //App Bar Back Button
              title: Text(widget.groupeName,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 17,
                  fontFamily: "Montserrat",
                ),
              ),
            ),
          body : StreamBuilder(
              stream: streamController.stream ,
              builder: (context, snapshot) {
                if(snapshot.hasData){
                  return ListView.builder(
                    itemCount: exercices.length,
                    itemBuilder: (context, index) {
                      return Card(
                          child: InkWell(
                            onTap: (){
                              Navigator.of(context).push(MaterialPageRoute
                                (builder: (BuildContext context) =>
                                  ExerciseScreen(exercice : exercices[index]),
                              ));
                            },
                            child:ListTile(
                              title: Text(exercices[index].title),
                            ) ,
                          )
                      );
                    },
                  );

                }
                else{
                  return Center(
                      child :
                      CircularProgressIndicator()
                  );
                }
              }
          ),
        );


      }

    }

