
import 'dart:async';

import 'package:cactus/model/GroupModel.dart';
import 'package:cactus/model/OrganisationModel.dart';
import 'package:cactus/service/forumService.dart';
import 'package:cactus/ui/NavDrawer.dart';
import 'package:cactus/ui/profileImageWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:developer';

import 'ForumList.dart';


class GroupListForum extends StatefulWidget {
  @override
  GroupListForumState createState() => GroupListForumState();
}

class GroupListForumState extends State<GroupListForum> {

  List<GroupModel> groupesList = [];
  List<OrganisationModel> listDataOrganisation;
  List<GroupModel> listGroupModel = [];
  StreamController streamController = StreamController();
  Widget groupListforForum(){
    try{
      Future<List<OrganisationModel>> dataOrganisation = getUserOrganisations();
      dataOrganisation.then((List<OrganisationModel> value) => {
        if(value != null){
          listDataOrganisation = value,

        }else{
          streamController.addError(Error),
        },
        groupesList = getGroupIdListFromOrganisation(listDataOrganisation),
        for (GroupModel group in groupesList){
          getUserGroup(group.id).then((value) => {listGroupModel.add(value),streamController.add(value)}),
        }
      });

    }catch(e){

    }

    return (
      Container(
        child: StreamBuilder(
          stream: streamController.stream ,
          builder: (context, snapshot) {
            if(snapshot.hasData){
              return ListView.builder(
                itemCount: listGroupModel.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.all(12),
                    child:Card(
                        child: InkWell(
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => ForumList(
                                  organisationId : listGroupModel[index].organisationId,
                                  groupId : listGroupModel[index].id,
                                ),
                              ),
                            );
                            print("Cliqué!");
                          },
                          child:ListTile(
                            title: Text(
                                listGroupModel[index].name
                            ),
                            subtitle: Text(listGroupModel[index].description),

                          ) ,
                        )
                    ) ,
                  );
                },
              );

            }
            if(snapshot.hasError){
              return Center(
                  child: Text("Aucune Donnée disponible")
              );
            }
            else{
              return Center(
                  child :
                  CircularProgressIndicator()
              );
            }
          }
        ),
      )
    );

  }
  @override
  Widget build(BuildContext context) {
    listGroupModel.clear();
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor:Color(0xFF4DD080),
        //App Bar Back Button
        title: Text("Community ",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          // Profile Image Widget
          ProfileImageWidget(),
        ],
      ),
      body: groupListforForum(),

    );


  }
}
