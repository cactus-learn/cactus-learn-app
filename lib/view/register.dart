import 'package:cactus/service/authService.dart';
import 'package:cactus/view/OnstartPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';
import 'package:loading_animations/loading_animations.dart';
import 'login.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class Registerpage extends StatefulWidget {
  @override
  _RegisterpageState createState() => _RegisterpageState();
}

final TextEditingController emailController = new TextEditingController();
final TextEditingController passwordController = new TextEditingController();
final TextEditingController familyNameController = new TextEditingController();
final TextEditingController lastNameController = new TextEditingController();
final TextEditingController birthdateController = new TextEditingController();


class _RegisterpageState extends State<Registerpage>{
  DateTime date2;

  @override
  bool _isLoading = false;
  void signUp(String familyName, String lastName, String email,String birthdate,String password) async {
    if(email == "" ||  lastName == "" || password == "" || familyName == "" || birthdate == ""){
      _showDialog("Champs vides", "Veuillez remplir tous les champs");
      return;
    }
    if(!EmailValidator.validate(email)){
      _showDialog("Champs invalides", "Votre email n'est pas valide");
      return;
    }
    setState(() {
      _isLoading = true;
    });

    var response = await register(familyName, lastName, email,birthdate ,password);
    if(response == 201){
      _showDialogsucces("braavoo", "un mail de virification à était envoyer  ");
      setState(() {
        _isLoading = false;
      });
    }
    else if(response == 400){
      _showDialog("Erreur", "Votre saisie est invalide");
    }
    else if(response == 409){
      _showDialog("Erreur", "Ce nom d'utilisateur est déjà utilisé");
    }
    else{
      _showDialog("Oups", "Une erreur s'est produite");
    }

    setState(() {
      _isLoading = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (BuildContext context) => OnstartPage()),
                    (Route<dynamic> route) => true);
          },
          icon: Icon (Icons.arrow_back_ios),
          color: Colors.green,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,

      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Image.asset(
                "assets/onboarding3.png",
              ),
            ),
            Positioned(
              top: 100,
              left: 32,
              child: Text('',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                ),
              ),
            ),

            Positioned(
              top: 190,
              child: Container(
                padding: EdgeInsets.all(32),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(62),
                        topRight: Radius.circular(62)
                    )
                ),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: familyNameController,
                      decoration: InputDecoration(
                        hintText: 'Nom',
                        icon: const Icon(Icons.supervised_user_circle),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 0),
                      child: TextField(
                        controller: lastNameController,
                        decoration: InputDecoration(
                          hintText: 'Prénom',
                          icon: const Icon(Icons.house_rounded),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 0),

                      child: DateTimePickerFormField(
                        inputType: InputType.date,
                        format: DateFormat("yyyy/MM/dd"),
                        initialDate: DateTime(2019, 1, 1),
                        editable: false,
                        decoration: InputDecoration(
                            icon: const Icon(Icons.date_range),
                            labelText: 'Date', hasFloatingPlaceholder: false),
                        controller: birthdateController,
                        onChanged: (dt) {
                          setState(() => date2 = dt);
                          print('Selected date: $date2');
                        },
                      ),

                    ),

                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 0),
                      child: TextField(
                        controller: emailController,
                        decoration: InputDecoration(
                          hintText: 'Email',
                          icon: const Icon(Icons.mail),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 62),
                      child: TextField(
                        controller: passwordController,
                        decoration: InputDecoration(
                          hintText: 'Mot de passe',
                          icon: const Icon(Icons.lock),
                        ),
                        obscureText: true,
                      ),
                    ),
                    !_isLoading ?
                    Container(
                      height: 45,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                            Radius.circular(32)
                        ),
                      ),
                      child: FlatButton(
                        onPressed: (){

                          signUp(familyNameController.text,lastNameController.text, emailController.text,birthdateController.text ,passwordController.text);
                        },
                        child: Text('S\'INSCRIRE',
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                      ),
                    ):
                    LoadingRotating.square(
                      borderColor: Colors.lightBlue,
                    ),
                    Container(height: 8),

                    Container(height: 70),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(width: 30),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                lastNameController.clear();
                familyNameController.clear();
                passwordController.clear();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }



  void _showDialogsucces(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          title: new Text(title, style: TextStyle(color: Colors.black),),
          content: new Text(content, style: TextStyle(color: Colors.black),),
          actions: <Widget>[
            new FlatButton(
              color: Colors.green[200],
              child: new Text("fermer", style: TextStyle(color: Colors.black),),
              onPressed: () {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (BuildContext context) => Loginpage()),
                        (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }

}