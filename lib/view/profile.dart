import 'package:cactus/model/userResponse.dart';
import 'package:cactus/service/UserService.dart';
import 'package:cactus/service/convService.dart';
import 'package:cactus/ui/NavDrawer.dart';
import 'package:cactus/ui/profileImageWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class ProfilePages extends StatefulWidget {
  @override
  _ProfilePagesState createState() => _ProfilePagesState();
}

class _ProfilePagesState extends State<ProfilePages> {


  UserResponse userResponse;
  TextEditingController controllerFirstName = TextEditingController();
  TextEditingController controllerLastName = TextEditingController();

  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerBirthDate = TextEditingController();

  TextEditingController controllerPassword = TextEditingController();
  TextEditingController controllerPassword2 = TextEditingController();

  setProfile() async{
    bool result = await updateUserFirstName(controllerFirstName.text);
    bool result2 = await updateUserLastName(controllerLastName.text);
    if(result && result2){
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Information utilisateur modifer avec succès"),
      ));
      setState(() {

      });
    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Erreur lors de la modification utilisateur"),
      ));

    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0.0,
        backgroundColor: Color(0xFF4DD080),
        //App Bar Back Button
        title: Text("Profil",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          // Profile Image Widget
          ProfileImageWidget(),
        ],
      ),

      body: FutureBuilder(
        future: getCurrentUserInfo(),
        builder: (BuildContext context, AsyncSnapshot<UserResponse> snapshot) {
          if(snapshot.connectionState == ConnectionState.done){
            userResponse = snapshot.data;
            controllerFirstName.text = userResponse.firstName;
            controllerLastName.text = userResponse.lastName;
            controllerEmail.text = userResponse.email;
            controllerBirthDate.text = userResponse.birthdate;
            return
              Center(
              child: Container(
                child: Wrap(
                  direction: Axis.horizontal,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child:  TextFormField(
                        controller: controllerFirstName,
                        decoration: InputDecoration(
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                            hintText: "Modifier votre Nom",
                            labelText: "Nom"
                        ),
                      ),
                    ),
                Padding(
                  padding:EdgeInsets.all(16.0) ,
                  child: TextFormField(
                    controller: controllerLastName,
                    decoration: InputDecoration(
                        border: new OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(10.0),
                          ),
                        ),
                        hintText: "Modifier votre Prénom",
                        labelText: "Prénom"
                    ),
                  ),
                ),
                    Padding(
                      padding: EdgeInsets.all(16.0) ,
                      child:TextField(
                        enabled: false,
                        controller: controllerEmail,
                        decoration: InputDecoration(
                            border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                const Radius.circular(10.0),
                              ),
                            ),
                            hintText: userResponse.email,
                            labelText: "Email"
                        ),
                      ) ,
                    ),
                    Padding(padding: EdgeInsets.all(16.0) ,
                    child:TextField(
                      enabled: false,
                      controller: controllerBirthDate,
                      decoration: InputDecoration(
                          border: new OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0),
                            ),
                          ),
                          hintText: userResponse.birthdate,
                          labelText: "Date de naissance"
                      ),) )
                    ,
                    Padding(padding:  EdgeInsets.all(16.0),
                      child: Center(
                        child:TextButton(
                          onPressed: (){setProfile();},
                          child: Text('Modifier le profil'),)
                        ,) ,
                      )
                  ],
                ),
              ),
            );
          }
          else{
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },

      ) ,

    );

  }


}
