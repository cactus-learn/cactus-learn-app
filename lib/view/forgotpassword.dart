import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'login.dart';

class Forgotpassword extends StatefulWidget {
  @override
  _ForgotpasswordState createState() => _ForgotpasswordState();
}

class _ForgotpasswordState extends State<Forgotpassword> {
  final TextEditingController emailController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        leading: IconButton(
          onPressed: (){
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (BuildContext context) => Loginpage()),
                    (Route<dynamic> route) => false);
          },
          icon: Icon (Icons.arrow_back_ios),
          color: Colors.green,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,

      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Image.asset(
                "assets/onboarding2.png",
              ),
            ),

            Positioned(
              top: 300,
              left: 32,
              child: Text('Connexion',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.red
                ),
              ),
            ),

            Positioned(
              top: 190,
              child: Container(
                padding: EdgeInsets.all(32),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(62),
                        topRight: Radius.circular(62)
                    )
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 20, bottom: 25),
                      child: TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                          hintText: 'Email',
                          icon: const Icon(Icons.supervised_user_circle),
                        ),

                      ),
                    ),

                    Container(
                      height: 45,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                            Radius.circular(32)
                        ),
                      ),
                      child:  FlatButton(
                        onPressed:()  {
                          //signIn(emailController.text, passwordController.text);
                        },
                        child: Text('Réinitialiser le mot de passe',
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                      ),

                    )

                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

