import 'dart:developer';

import 'package:cactus/model/NoteModel.dart';
import 'package:cactus/model/questionModel.dart';
import 'package:cactus/service/NoteService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Notes extends StatefulWidget {
  @override
  _NotesState createState() => _NotesState();
}

class _NotesState extends State<Notes> {

  Widget displayResponse(QuestionModel model){
    return Card(

      child:Column(
        children: <Widget>[
          ListTile(
            title: Text(model.text),
          ),
          ListView.builder(
            shrinkWrap: true,
            itemCount: model.responsesModel.length,
            itemBuilder: (context, index) {


              return Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(model.responsesModel[index].text, style: TextStyle(color: model.responsesModel[index].isAnswer ? Colors.green : Colors.redAccent),),
                      Icon(model.responsesModel[index].isUserResponse ? Icons.done : null),
                    ],)
              );
            },)
        ],
      ),


    );
  }
  popUpNote(NoteModel model){
    return showDialog(
        context: context,
        builder: (BuildContext context){
          return Center(
            child: AlertDialog(
                contentPadding: EdgeInsets.only(left: 25, right: 25),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
                title: Text(model.exercicesModel.title),
                content: Container(
                  height: 350,
                  width: 300,
                  child: Column(
                    children: [
                      Text(model.groupModel.name),
                      SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child :
                        SizedBox(
                          height: 200,
                          child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: model.responseModel.questions.length,
                            itemBuilder: (context, index) {
                              return displayResponse(model.responseModel.questions[index]);
                            },),

                        ),
                      ),

                      ButtonBar(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          TextButton(onPressed: () { Navigator.pop(context); },
                            child: Text("Fermer"),)
                        ],)
                    ],
                  ),
                )
            ),
          );
        }
    );
  }


  Widget noteWidget(){
    return FutureBuilder(
      future: getUserNote(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if(snapshot.connectionState == ConnectionState.done){
          if(snapshot.hasData){
            List<NoteModel> notes = snapshot.data;
            return ListView.builder(
              itemCount : notes.length,
              itemBuilder: (context, index) {
                return Padding(padding: EdgeInsets.all(18),
                    child: Card(
                      child: InkWell(
                        onTap: () {
                        },
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(notes[index].exercicesModel.title),
                              subtitle: Text(notes[index].groupModel.name),
                            ),
                            Text(notes[index].responseModel.marks.toString()+"/"+notes[index].responseModel.total.toString(),
                              style:TextStyle (fontSize: 20, color: Colors.redAccent) ,),
                            ButtonBar(children: [IconButton(onPressed: () {
                              popUpNote(notes[index]);

                            }, icon: Icon(Icons.info, color: Colors.grey,))],)
                          ],
                        ),
                      ),
                    ));

              },);
          }
          else{
            return Text("Pas de donnée");
          }

        }
        else{
          return Center(child: CircularProgressIndicator(),);
        }

      },
    );
  }

//  fetchData() async {
//    List<NoteModel> notes = await getUserNote();
//    log("LOG:"+notes.length.toString());
//  }
//  @override
//  void initState() {
//    // TODO: implement initState
//   fetchData();
//    super.initState();
//  }
//
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:noteWidget(),
    );
  }
}
