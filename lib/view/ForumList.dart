import 'dart:async';
import 'dart:developer';

import 'package:cactus/model/ForumModel.dart';
import 'package:cactus/model/UserDetails.dart';
import 'package:cactus/service/UserService.dart';
import 'package:cactus/service/forumService.dart';
import 'package:cactus/ui/NavDrawer.dart';
import 'package:cactus/ui/profileImageWidget.dart';
import 'package:cactus/utils/AlertDialogDesign.dart';
import 'package:cactus/utils/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'Forum.dart';

class ForumList extends StatefulWidget {
  final String groupId;
  final String organisationId;

  final Function(ForumModel forum) parentFunction;
  ForumList({this.groupId, this.organisationId, this.parentFunction});

  bool isAuthor = false;


  @override
  ForumListState createState() => ForumListState();
}

class ForumListState extends State<ForumList>{

  Future<List<ForumModel>> listForumModel;
  List<ForumModel> listData = [];
  StreamController streamController = StreamController();
  final _formKey = GlobalKey<FormState>();
  String newForumName = "";

 popUpCreateForum(){
    return showDialog(
      context: context,
      builder: (BuildContext context){
        return Center(
          child: AlertDialog(
            content: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  height: 200,
                  child:   Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: TextFormField(
                            validator: (String value){
                              if(value == null || value.isEmpty){
                                return 'Inserer le nom du forum';
                              }
                              return null;
                            },
                            onChanged: (String value){
                              newForumName = value;

                            },
                            decoration:
                            InputDecoration(
                              labelText: 'Nom du Forum'
                            )
                            ,) ,
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),

                          child: Center(
                            child: ButtonBar(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                TextButton(
                                  onPressed: () async => { if (_formKey.currentState.validate()) {
                                    await createForum(newForumName, this.widget.groupId, this.widget.organisationId).then((ForumModel value) => {
                                      if(value != null ) {
                                        log("Ok"),
                                        Navigator.pop(context),
                                        setState((){listData.add(value);
                                        }),

                                      }
                                      else{
                                        log("Erreur")
                                      }
                                    })
                                  }},
                                  child: Text("Ok"),
                                ),
                                TextButton(
                                    onPressed: () =>Navigator.pop(context),
                                    child:Text("Fermer")
                                )
                              ],
                            ),
                          )
                        )
                      ],
                    ),

                  ),
                )

              ],
            ),
          ),
        );
      }
    );
  }

  popUpDeleteConfirmation(ForumModel model){
     return showDialog(
       context: context,
       builder: (BuildContext context){
         return Center(
           child: AlertDialog(
             content: Stack(
               children: <Widget>[
                 Container(
                   height: 150,
                   child: Center(
                     child :
                     Column(
                       children: [
                         Text("Vous êtes sur le point de supprimer le forum :\n"+model.name),
                         ButtonBar(
                           mainAxisSize: MainAxisSize.min,
                           children: [
                             TextButton(
                               onPressed: () async => {
                                 await deleteForum(model.id).then((value) => {
                                   if (value!= null){
                                     setState((){
                                       listData.remove(model);
                                       Navigator.pop(context);
                                     })
                                   }else{
                                     showDialog(
                                       context: context,
                                       builder: (context){
                                         return AlertDialogDesign.badAlertDialog(context, "Problème lors de la suppression du forum", "Une erreur est survenu");
                                       },
                                     )
                                   }
                                 })
                               },
                               child: Text("Oui"),),
                             TextButton(onPressed: () { Navigator.pop(context); },
                               child: Text("Non"),)

                           ],)
                       ],
                     )
                   )
                 )


               ],
             )
           ),
         );
       }
     );
  }

  @override
  Widget build(BuildContext context) {
    listForumModel = getForumsFromGroup(this.widget.groupId);
    listForumModel.then((value) => {listData = value, streamController.add(listData)});

    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        elevation: 0.0,
        backgroundColor: Color(0xFF4DD080),
        //App Bar Back Button
        title: Text("Mon forum ",
          style: TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontFamily: "Montserrat",
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          // Profile Image Widget
          ProfileImageWidget(),

        ],
      ),
      body: Container(
        child: StreamBuilder(
          stream: streamController.stream,
          builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
            if(snapshot.hasData){
              return ListView.builder(
                itemCount: listData.length,
              itemBuilder: (context, index){
                  return ForumCard(forum: listData[index], fonctionParent: popUpDeleteConfirmation);
              });
            }
            else{
              return Center(
                  child :
                  CircularProgressIndicator()
              );
            }

          },
        ),
      ),
    floatingActionButton: FloatingActionButton.extended(
      onPressed: () => popUpCreateForum(),
      label: Text('Creer un forum !'),
    ),);
  }

  removeForumFromList(ForumModel forum){
     listData.remove(forum);
  }
}

class ForumCard extends StatefulWidget{

  ForumModel forum;
  final Function(ForumModel forumModel) fonctionParent;

  ForumCard({this.forum, this.fonctionParent});

  @override
  _ForumCardState createState() => _ForumCardState();
}

class _ForumCardState extends State<ForumCard> {

  bool _isAuthor = false;
  User author;


  generateAuthorButton(){
    if(_isAuthor){
      return ButtonBar(
        children: [
          Padding(
            padding:  EdgeInsets.all(16.0),
            child: IconButton(
              icon: widget.forum.statut_close ? Icon(Icons.lock, color: Colors.redAccent,) : Icon(Icons.lock_open, color: Colors.green,),
              onPressed: () async => {
                if(widget.forum.statut_close){
                  await openForum(widget.forum.id).then((value) => {
                    if(value == true){
                      setState((){
                        widget.forum.statut_close=!widget.forum.statut_close;
                      }),
                    }
                    else{
                      showDialog(
                      context: context,
                      builder: (context){
                        return AlertDialogDesign.badAlertDialog(context, "Problème lors de la réouverture du forum", "Une erreur est survenu");
                      },
                      )
                    }
                  })
                }
                else{
                  await closeForum(widget.forum.id).then((value) => {
                    if(value == true){
                      setState((){
                        widget.forum.statut_close=!widget.forum.statut_close;
                      }),
                    }
                    else{
                      showDialog(
                        context: context,
                        builder: (context){
                        return AlertDialogDesign.badAlertDialog(context, "Problème lors de la fermeture du forum", "Une erreur est survenu");
                        },
                      )
                    }
                  })
                }
              },

            ),
          ),
          IconButton(icon: Icon(Icons.cancel, color: Colors.redAccent,), onPressed: () {widget.fonctionParent(widget.forum);}
            ,)
        ],
      );
    }else {
      return SizedBox.shrink();
    }
  }

  @override
  void initState() {
    getUserInfo(widget.forum.creatorId).then((value){
      setState(() {
        author = value;
        if(author.id == widget.forum.creatorId){
          _isAuthor = true;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {


    return Card(
      elevation: 2,
      child: InkWell(
        onTap: (){
          Navigator.pushNamed(
              context, Forum.routeName,arguments: widget.forum
          );
        },
        child: Column(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(12),
            child: Text(widget.forum.name,

              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),),
            Text(author.lastName +" "+ author.firstName,
              style: TextStyle(
                  fontSize: 15)),
            generateAuthorButton()
          ],
        ) ,
      )
    );
  }

}