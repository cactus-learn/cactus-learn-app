import 'package:cactus/service/authService.dart';
import 'package:cactus/view/register.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loading_animations/loading_animations.dart';
import 'forgotpassword.dart';
import 'home.dart';

class Loginpage extends StatefulWidget {
  @override
  _LoginpageState createState() => _LoginpageState();
}
final TextEditingController emailController = new TextEditingController();
final TextEditingController passwordController = new TextEditingController();
class _LoginpageState extends State<Loginpage>{
  bool _isLoading = false;
  void _showDialog(String title, String content) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(content),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                emailController.clear();
                passwordController.clear();

                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  signIn(String email, String password) async {
    if(email == "" || password == ""){
      _showDialog("Champs vides", "Veuillez remplir tous les champs");
      return;
    }
    setState(() {
      _isLoading = true;
    });

    var response = await auth(email, password);

    if(response == 500){
      print(response);
      _showDialog("Oups", "Votre compte n'est pas activé veuillez l'activer SVP");
    }
    else if(response == 200 || response == 201){
      print(response);

      emailController.clear();
      passwordController.clear();
      setState(() {
        _isLoading = false;
      });

      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => HomePage()),
              (Route<dynamic> route) => false);
    }
    else if(response == 403 || response == 401){
      _showDialog("ERREUR", "Vos identifiants sont incorrects");
    }

    setState(() {
      _isLoading = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        leading: IconButton(

          onPressed: (){
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (BuildContext context) => Registerpage()),
                    (Route<dynamic> route) => false);
          },
          icon: Icon (Icons.arrow_back_ios),
          color: Colors.green,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,

      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Stack(
          children: <Widget>[
            Positioned(
              child: Image.asset(
                "assets/onboarding2.png",
              ),
            ),

            Positioned(
              top: 300,
              left: 32,
              child: Text('Connexion',
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Colors.red
                ),
              ),
            ),

            Positioned(
              top: 190,
              child: Container(
                padding: EdgeInsets.all(32),
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(62),
                        topRight: Radius.circular(62)
                    )
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 0),
                      child: TextFormField(
                        controller: emailController,
                        decoration: InputDecoration(
                          hintText: 'Email',
                          icon: const Icon(Icons.supervised_user_circle),
                        ),

                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.only(top: 16, bottom: 62),
                      child: TextFormField(
                        controller: passwordController,
                        decoration: InputDecoration(
                          hintText: 'Mot de passe',
                          icon: const Icon(Icons.lock),

                        ),
                        obscureText: true,
                      ),
                    ),

                    !_isLoading ?
                    Container(
                      height: 45,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                            Radius.circular(32)
                        ),
                      ),
                      child:  FlatButton(
                        onPressed:()  {
                          signIn(emailController.text, passwordController.text);
                        },
                        child: Text('SE CONNECTER',
                          style: TextStyle(
                              color: Colors.white
                          ),
                        ),
                      ),

                    ):
                    LoadingRotating.square(
                      borderColor: Colors.lightBlue,
                    ),
                    Container(
                      height: 45,
                      width: double.maxFinite,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                            Radius.circular(32)
                        ),
                      ),
                      child:  FlatButton(
                          onPressed:() {
                            Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (context)=>Forgotpassword()
                                )
                            );
                        },
                        child: Text('Mot de passe oublier ',
                          style: TextStyle(
                              color: Colors.blue
                          ),
                        ),
                      ),

                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}