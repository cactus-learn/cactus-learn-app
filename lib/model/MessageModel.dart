import 'package:cactus/model/userResponse.dart';

class MessageModel{

  bool read;
  bool isLiked;
  String sentAt;
  String id;
  String content ;
  String receiverId;
  UserResponse receiverInfo ;
  String sender;

  MessageModel({this.read, this.isLiked, this.sentAt, this.id, this.content,this.receiverId,
      this.receiverInfo, this.sender});


  MessageModel.jsonData({this.read, this.isLiked, this.sentAt, this.id, this.content,this.receiverId,
    this.receiverInfo, this.sender});

  factory MessageModel.fromJsonData(Map<String,  dynamic> json){
    return MessageModel.jsonData(
      id: json['id'],
      read: json['read'],
      isLiked: json['isLiked'],
      sentAt: json['sentAt'],
      content: json['content'],
      receiverId : json['receiverId'],
      receiverInfo:  UserResponse.fromJson(json['receiverInfo']),
      sender: json['sender'],

    );
  }
}

