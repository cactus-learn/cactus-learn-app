import 'package:cactus/model/GroupModel.dart';
import 'package:cactus/model/exercicesMdel.dart';

import 'ResponseModel.dart';

class NoteModel{

  ExercicesModel exercicesModel;
  GroupModel groupModel;
  ResponseModel responseModel;


  NoteModel({this.exercicesModel, this.groupModel, this.responseModel});


  NoteModel.jsonData({this.exercicesModel, this.groupModel, this.responseModel});

  factory NoteModel.fromJsonData(Map<String,  dynamic> json){
    return NoteModel.jsonData(
      exercicesModel: ExercicesModel.fromJsonNoteData(json['exercise']),
      groupModel: GroupModel.fromJsonNoteData(json['groupe']),
      responseModel: ResponseModel.fromJsonData(json['response']),


    );
  }
}

