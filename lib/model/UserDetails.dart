class User {
  final String id;
  String name;
  String imageUrl;
  String firstName;
  String lastName;
  String email;

  User({
    this.id,
    this.name,
    this.imageUrl,
  });

  User.jsonData({ this.id,this.firstName,this.lastName, this.email});


  factory User.fromJsonData(Map<String, dynamic> json){
    return User.jsonData(
      id: json['_id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['email'],
    );
  }

  Map<String, String> toJson() =>
      {
      };

  @override
  String toString() {
    return 'User{id: $id, firstName: $firstName, lastName: $lastName, email: $email}';
  }


}
