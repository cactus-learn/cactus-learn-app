import 'exercicesMdel.dart';

class ExerciceGlobal {

  String groupeId;
  String groupeName;
  List<ExercicesModel> exercises;



  ExerciceGlobal({this.groupeId, this.groupeName, this.exercises });

  ExerciceGlobal.jsonData({this.groupeId, this.groupeName, this.exercises});

  factory ExerciceGlobal.fromJsonData(Map<String,  dynamic> json){
    return ExerciceGlobal.jsonData(
      groupeId: json['groupeId'],
      groupeName: json['groupeName'],
      exercises:  (json['exercises'] as List).map((e) => ExercicesModel.fromJsonData(e)).toList(),

    );
  }

  ExerciceGlobal.jsonDataexercice({this.groupeName, this.exercises});


  factory ExerciceGlobal.fromJsonDataExercice(Map<String,  dynamic> json){
    return ExerciceGlobal.jsonDataexercice(
      groupeName: json['groupeName'],
      exercises:  (json['exercises'] as List).map((e) => ExercicesModel.fromJsonData(e)).toList(),

    );
  }
}