import 'dart:core';

import 'package:cactus/model/PostModel.dart';
import 'package:cactus/model/questionModel.dart';


class ExercicesModel{

  String id ;
  String title;
  String creator;
  List<QuestionModel> questions;


  ExercicesModel({this.id, this.title, this.questions ,this.creator});

  ExercicesModel.jsonData({this.id, this.title, this.questions,this.creator});

  ExercicesModel.jsonId({this.id});

  ExercicesModel.jsonNoteData({this.id,this.title});


  factory ExercicesModel.fromJsonData(Map<String,  dynamic> json){
    return ExercicesModel.jsonData(
      id: json['_id'],
      title: json['title'],
      creator: json['creator'],
      questions:  (json['questions'] as List).map((e) => QuestionModel.fromJsonData(e)).toList(),

    );
  }

  factory ExercicesModel.fromJsonId(Map<String,  dynamic> json){
    return ExercicesModel.jsonId(
      id: json['_id'],
    );
  }

  factory ExercicesModel.fromJsonNoteData(Map<String,  dynamic> json){
    return ExercicesModel.jsonNoteData(
      id: json['_id'],
      title:  json['title'],
    );
  }

  @override
  String toString() {
    return 'ExercicesModel{id: $id, title: $title, creator: $creator, questions: $questions}';
  }
}



