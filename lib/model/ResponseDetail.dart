import 'package:cactus/model/GroupModel.dart';
import 'package:cactus/model/exercicesMdel.dart';
import 'package:cactus/model/userResponse.dart';

class ResponseDetail{

  String id;
  String text ;
  bool isAnswer;
  bool isUserResponse;

  ResponseDetail({ this.id, this.text,this.isAnswer, this.isUserResponse});
  ResponseDetail.jsonData({ this.id, this.text,this.isAnswer, this.isUserResponse});

  factory ResponseDetail.fromJsonData(Map<String,  dynamic> json){
    return ResponseDetail.jsonData(
      id: json['_id'],
      text: json['text'],
      isAnswer : json['isAnswer'],
      isUserResponse: json['isUserResponse'],
    );
  }
}

