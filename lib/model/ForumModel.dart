import 'dart:core';

import 'package:cactus/model/PostModel.dart';


class ForumModel{

  String id;
  bool statut_close;
  String createdAt;
  String name;
  String groupeId ;
  String organisationId ;
  String creatorId;
  List<dynamic> posts;
  List<PostModel> postModels;


  ForumModel({this.id, this.statut_close, this.createdAt, this.name,
      this.groupeId, this.organisationId, this.creatorId, this.posts});

  ForumModel.jsonData({this.id, this.statut_close, this.createdAt, this.name, this.groupeId, this.organisationId, this.creatorId, this.posts});

  ForumModel.jsonId({this.id});

  ForumModel.jsonDataPostModel({this.id, this.statut_close, this.createdAt, this.name, this.groupeId, this.organisationId, this.creatorId, this.postModels});


  factory ForumModel.fromJsonData(Map<String,  dynamic> json){
    return ForumModel.jsonData(
      id: json['_id'],
      statut_close: json['statut_close'],
      name: json['name'],
      groupeId: json['groupeId'],
      organisationId: json['organisationId'],
      creatorId : json['creatorId'],
      posts:  json['posts'],

    );
  }

  factory ForumModel.fromJsonDataWithPostModel(Map<String,  dynamic> json){

    return ForumModel.jsonDataPostModel(
      id: json['_id'],
      statut_close: json['statut_close'],
      name: json['name'],
      groupeId: json['groupeId'],
      organisationId: json['organisationId'],
      creatorId : json['creatorId'],
      postModels:  (json['posts'] as List).map((e) => PostModel.fromJsonData(e)).toList(),

    );
  }
  factory ForumModel.fromJsonId(Map<String,  dynamic> json){
    return ForumModel.jsonId(
      id: json['_id'],
    );
  }
}

