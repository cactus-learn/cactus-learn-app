import 'dart:core';


class GroupModel{

  String id;
  List<dynamic> exercises;
  String name;
  String creatorId;
  String organisationId;
  String description ;
  List<dynamic> students ;


  GroupModel({this.id, this.exercises, this.name, this.creatorId,
      this.organisationId, this.description, this.students});

  GroupModel.jsonData({this.id, this.exercises, this.name, this.creatorId, this.description, this.organisationId, this.students});

  GroupModel.jsonId({this.id});

  GroupModel.jsonNoteData({this.id,this.name});


  factory GroupModel.fromJsonData(Map<String,  dynamic> json){
    return GroupModel.jsonData(
      id: json['_id'],
      exercises: json['exercises'],
      name: json['name'],
      creatorId: json['creatorId'],
      description: json['description'],
      organisationId : json['organisationId'],
      students:  json['students'],

    );
  }

  factory GroupModel.fromJsonId(Map<String,  dynamic> json){
    return GroupModel.jsonId(
      id: json['groupeId'],
    );
  }

  factory GroupModel.fromJsonNoteData(Map<String,  dynamic> json){
    return GroupModel.jsonNoteData(
      id: json['_id'],
      name: json['name'],
    );
  }
}

