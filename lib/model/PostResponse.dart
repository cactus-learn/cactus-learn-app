import 'dart:core';

import 'package:cactus/model/PostModel.dart';
import 'package:cactus/model/userResponse.dart';


class PostResponse{

  PostModel post;
  UserResponse userInfo;


  PostResponse({this.post, this.userInfo});

  PostResponse.jsonData({this.post, this.userInfo});


  factory PostResponse.fromJsonData(Map<String,  dynamic> json){
    return PostResponse.jsonData(
      post: PostModel.fromJsonData(json['post']),
      userInfo: UserResponse.fromJsonPost(json['userInfo']),


    );
  }


}

