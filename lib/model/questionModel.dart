class QuestionModel {



  String id;
  String questionType;
  int coefficient;
  String text;
  List<dynamic> responses;
  List<ResponsesModel> responsesModel;


  QuestionModel({this.id, this.questionType, this.coefficient,this.text});
  QuestionModel.jsonData({this.id, this.questionType, this.coefficient,this.text, this.responsesModel});

  QuestionModel.jsonNoteData({this.id, this.questionType, this.coefficient,this.text, this.responsesModel});


  QuestionModel.jsonId({this.id});

  QuestionModel.jsonDataResponse({this.id, this.questionType, this.coefficient,this.text});


  Map<String, dynamic> toJson() => {
    'id': id,
    'questionType': questionType,
    'text': text,
    'coefficient': coefficient,
    'responses': responsesModel,
  };


  factory QuestionModel.fromJsonData(Map<String,  dynamic> json){
    return QuestionModel.jsonData(
      id: json['_id'],
      questionType: json['questionType'],
      coefficient: json['coefficient'],
      text: json['text'],
      responsesModel:  (json['responses'] as List).map((e) => ResponsesModel.fromJsonData(e)).toList(),
    );
  }


  factory QuestionModel.fromJsonId(Map<String,  dynamic> json){
    return QuestionModel.jsonId(
      id: json['_id'],
    );
  }

  factory QuestionModel.fromJsonNoteData(Map<String,  dynamic> json){
    return QuestionModel.jsonData(
      id: json['_id'],
      questionType: json['questionType'],
      coefficient: json['coefficient'],
      text: json['text'],
      responsesModel:  (json['responses'] as List).map((e) => ResponsesModel.fromJsonDataNote(e)).toList(),
    );
  }

}

class ResponsesModel {
  String id;
  String text;
  bool isAnswer;
  bool isUserResponse;


  ResponsesModel({this.id, this.text, this.isAnswer});

  ResponsesModel.jsonData({this.id, this.text, this.isAnswer});
  ResponsesModel.jsonDataNote({this.id, this.text, this.isAnswer,this.isUserResponse});

    Map<String, dynamic> toJson() => {
      'id': id,
      'text': text,
      'isAnswer': isAnswer,
      'isUserResponse': isUserResponse,
    };

  factory ResponsesModel.fromJsonData(Map<String,  dynamic> json){
    var response = ResponsesModel.jsonData(
      id: json['_id'],
      text: json['text'],
      isAnswer: json['isAnswer']);
    response.isUserResponse = false;
    return response;
  }

  factory ResponsesModel.fromJsonDataNote(Map<String,  dynamic> json){
    return ResponsesModel.jsonDataNote(
      id: json['_id'],
      text: json['text'],
      isAnswer: json['isAnswer'],
      isUserResponse :json['isUserResponse'],
    );
  }

  @override
  String toString() {
    return 'ResponsesModel{id: $id, text: $text, isAnswer: $isAnswer, isUserResponse: $isUserResponse}';
  }
}
