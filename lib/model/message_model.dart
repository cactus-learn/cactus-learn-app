import 'UserDetails.dart';

class Message {
  final User sender;
  final String
  time; // Would usually be type DateTime or Firebase Timestamp in production apps
  final String text;
  final bool isLiked;
  final bool unread;

  Message({
    this.sender,
    this.time,
    this.text,
    this.isLiked,
    this.unread,
  });
}

// YOU - current user
final User currentUser = User(
  id: '0',
  name: 'Current User',
  imageUrl: 'assets/profile/greg.jpg',
);

// USERS
final User greg = User(
  id: '1',
  name: 'Greg',
  imageUrl: 'assets/profile/greg.jpg',
);
final User james = User(
  id: '2',
  name: 'James',
  imageUrl: 'assets/profile/james.jpg',
);
final User jhon = User(
  id: '3',
  name: 'John',
  imageUrl: 'assets/profile/jhon.jpg',
);
final User olivia = User(
  id: '4',
  name: 'Olivia',
  imageUrl: 'assets/profile/olivia.jpg',
);
final User sam = User(
  id: '5',
  name: 'Sam',
  imageUrl: 'assets/profile/sam.jpg',
);
final User sophia = User(
  id: '6',
  name: 'Sophia',
  imageUrl: 'assets/profile/sophia.jpg',
);
final User steven = User(
  id: '7',
  name: 'Steven',
  imageUrl: 'assets/profile/steven.jpg',
);

// FAVORITE CONTACTS

// EXAMPLE CHATS ON HOME SCREEN
List<Message> chats = [
  Message(
    sender: james,
    time: '5:30 PM',
    text: 'weshhhhh',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: olivia,
    time: '4:30 PM',
    text: 'toi weshhh',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: jhon,
    time: '3:30 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: sophia,
    time: '2:30 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: steven,
    time: '1:30 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: sam,
    time: '12:30 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: false,
  ),
  Message(
    sender: greg,
    time: '11:30 AM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: false,
  ),
];

// EXAMPLE MESSAGES IN CHAT SCREEN
List<Message> messages = [
  Message(
    sender: james,
    time: '5:30 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: true,
    unread: true,
  ),
  Message(
    sender: currentUser,
    time: '4:30 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: james,
    time: '3:45 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: james,
    time: '3:15 PM',
    text: 'All the food',
    isLiked: true,
    unread: true,
  ),
  Message(
    sender: currentUser,
    time: '2:30 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: true,
  ),
  Message(
    sender: james,
    time: '2:00 PM',
    text: 'wesh weshhhhhhhh',
    isLiked: false,
    unread: true,
  ),
];
