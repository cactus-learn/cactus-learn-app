import 'package:cactus/model/GroupModel.dart';
import 'package:cactus/model/exercicesMdel.dart';
import 'package:cactus/model/questionModel.dart';
import 'package:cactus/model/userResponse.dart';

class ResponseModel{


  String sendAt;
  String id ;
  String exerciseId;
  List<QuestionModel> questions;

  int total;
  int coefficient ;
  String userId;
  dynamic marks;

  ResponseModel({this.sendAt, this.id,this.exerciseId, this.questions,this.total,this.marks,this.coefficient, this.userId});
  ResponseModel.jsonData({this.sendAt, this.id,this.exerciseId, this.questions,this.total,this.marks,this.coefficient, this.userId});

  factory ResponseModel.fromJsonData(Map<String,  dynamic> json){
    return ResponseModel.jsonData(
      id: json['_id'],
      sendAt: json['sendAt'],
      exerciseId : json['exerciseId'],
      questions: (json['questions'] as List).map((e) => QuestionModel.fromJsonNoteData(e)).toList(),
      total: json['total'],
      marks: json['marks'],
      coefficient : json['coefficient'],
      userId: json['userId'],


    );
  }
}

