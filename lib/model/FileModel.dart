import 'dart:core';


class FileModel{

  String src;
  String name;
  String type ;

  FileModel({this.src,this.name, this.type});

  FileModel.jsonData({this.src,this.name, this.type});

  factory FileModel.fromJsonData(Map<String,  dynamic> json){
    return FileModel.jsonData(
      src: json['src'],
      name: json['name'],
      type: json['type'],
    );
  }



}


