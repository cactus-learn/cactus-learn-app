class UserResponse{

  String id;
  String firstName;
  String lastName;
  String email;
  String birthdate;

  UserResponse(
  {this.id, this.firstName, this.lastName, this.email, this.birthdate});

  UserResponse.NoId(
      this.firstName, this.lastName, this.email, this.birthdate);


  void setId(String id) => this.id = id;
  UserResponse.jsonData({this.firstName,this.lastName, this.email, this.birthdate});
  UserResponse.jsonDataPost({this.id, this.firstName,this.lastName, this.email});

  UserResponse.jsonDataId({this.id, this.firstName,this.lastName, this.email, this.birthdate});

  factory UserResponse.fromJson(Map<String,  dynamic> json){
    return UserResponse.jsonData(

      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['email'],
      birthdate: json['birthdate'],

    );
  }

  factory UserResponse.fromJsonPost(Map<String,  dynamic> json){
    return UserResponse.jsonDataPost(
      id : json['_id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['email'],


    );
  }




  factory UserResponse.fromJsonWithId(Map<String,  dynamic> json){
    return UserResponse.jsonDataId(
      id : json['_id'],
      firstName: json['firstName'],
      lastName: json['lastName'],
      email: json['email'],
      birthdate: json['birthdate'],

    );
  }

  factory UserResponse.fromJsonOnlyId(Map<String,  dynamic> json){
    return UserResponse.jsonDataId(
      id : json['userId'],
    );
  }

}
