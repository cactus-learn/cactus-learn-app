import 'dart:core';

import 'package:cactus/model/FileModel.dart';
import 'package:cactus/model/PostModel.dart';


class ChapterModel{

  String id;
  String title;
  String content;
  FileModel fileModel;

  ChapterModel({this.id, this.title,this.content, this.fileModel});

  ChapterModel.jsonData({this.id, this.title,this.content, this.fileModel});

  factory ChapterModel.fromJsonData(Map<String,  dynamic> json){
    var chapter = ChapterModel.jsonData(
      id: json['_id'],
      title: json['title'],
      content: json['content']
    );

    if(json['file']!=null) chapter.fileModel = FileModel.fromJsonData(json['file']);

    return chapter;
  }



}


