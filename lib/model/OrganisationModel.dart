import 'dart:core';


class OrganisationModel{

  String id;
  String cratedAt;
  String name;
  String creatorId;
  List<dynamic> admins ;
  List<dynamic> members;
  List<dynamic> groupes ;


  OrganisationModel(this.id, this.cratedAt, this.name, this.creatorId,
      this.admins, this.members, this.groupes);

  OrganisationModel.jsonData({this.id, this.cratedAt, this.name, this.creatorId,
      this.admins, this.members, this.groupes});

  factory OrganisationModel.fromJsonData(Map<String,  dynamic> json){
    return OrganisationModel.jsonData(
      id: json['_id'],
      cratedAt: json['cratedAt'],
      name: json['name'],
      creatorId: json['creatorId'],
      admins: json['admins'],
      members : json['members'],
      groupes:  json['groupes'],

    );
  }
}

