import 'dart:core';

import 'package:cactus/model/ChapterModel.dart';
import 'package:cactus/model/FileModel.dart';
import 'package:cactus/model/PostModel.dart';


class LessonModel{

  /*
     "_id": "60d8f5173cb9cc8054bd3331",
            "title": "Es",
            "content": "zeeaze",
            "files": [
                {
                    "_id": "60d8f51f3cb9cc8054bd333e",
                    "src": "https://cucfilesystemtorage.blob.core.windows.net/file-system/me_1624815842618_1624831257094.jpg",
                    "name": "me_1624815842618.jpg",
                    "type": "image"
                }
            ]
     */
  String id;
  String title;
  String creator;
  List<ChapterModel> chapterModels;

  LessonModel({this.id, this.title,this.creator, this.chapterModels});

  LessonModel.jsonData({this.id, this.title,this.creator, this.chapterModels});

  factory LessonModel.fromJsonData(Map<String,  dynamic> json){
    return LessonModel.jsonData(
      id: json['_id'],
      title: json['title'],
      creator: json['creator'],
      chapterModels: (json['chapters'] as List).map((e) => ChapterModel.fromJsonData(e)).toList(),
    );
  }



}


