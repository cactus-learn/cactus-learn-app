import 'dart:core';

import 'package:cactus/model/userResponse.dart';


class PostModel{

  String id;
  String createdAt;
  String forumId;
  String creatorId;
  String content;
  List<UserResponse> thumb_up ;
  List<UserResponse> thumb_down ;
  //Todo Ajouter Creator Name


  PostModel({this.id, this.createdAt, this.forumId, this.creatorId, this.content,
      this.thumb_up, this.thumb_down});

  PostModel.jsonData({this.id, this.createdAt, this.forumId, this.creatorId, this.content, this.thumb_up, this.thumb_down});

  PostModel.jsonId({this.id});
  PostModel.jsonDataResponse({this.id, this.createdAt, this.forumId, this.creatorId, this.content, this.thumb_up, this.thumb_down});


  factory PostModel.fromJsonData(Map<String,  dynamic> json){
    return PostModel.jsonData(
      id: json['_id'],
      createdAt: json['createdAt'],
      forumId: json['forumId'],
      creatorId: json['creatorId'],
      content: json['content'],
      thumb_up : (json['thumb_up'] as List).map((e) => UserResponse.fromJsonOnlyId(e)).toList(),
      thumb_down:  (json['thumb_down'] as List).map((e) => UserResponse.fromJsonOnlyId(e)).toList(),

    );
  }




  factory PostModel.fromJsonId(Map<String,  dynamic> json){
    return PostModel.jsonId(
      id: json['postId'],
    );
  }
}

