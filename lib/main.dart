import 'package:cactus/view/Forum.dart';
import 'package:cactus/view/OnstartPage.dart';
import 'package:cactus/view/VideoView.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cactus',
      debugShowCheckedModeBanner: false,
      routes: {
        "/forum" : (context) => Forum(),
        "/video" : (context) => VideoView(),

    },
      home: OnstartPage(),
    );
  }
}
