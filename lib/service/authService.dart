import 'dart:convert';
import 'dart:developer';
import 'package:cactus/config/config.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart'as http;
auth(String email, String password) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var jsonResponse = null;

  Map data = {
    'email': email,
    'password': password
  };
  var response;
  try {
    //10.0.2.2 for android emulator
    response = await http.post(url + "/auth/login",
        headers: {"Content-type": "application/json"}, body: json.encode(data));

  }
  catch (e) {
    print(e.toString());
    return 500;
  }
  if(response.statusCode == 200) {
    jsonResponse = json.decode(response.body);
    if(jsonResponse != null) {
      sharedPreferences.setString("token", jsonResponse['token']);
      Map<String, dynamic> decodedToken = JwtDecoder.decode(jsonResponse['token']);
      sharedPreferences.setString("id", decodedToken['_id']);
    }
    return 200;
  }
  else if(response.statusCode == 403) {
    return 403;
  }
  else {
    log(response.body);
    return 500;
  }
}


register(String familyName, String lastName, String email,String birthdate, String password) async {
  Map data = {
    "firstName": familyName,
    "lastName": lastName,
    "email":email,
    "birthdate":birthdate,
    "password":password
  };
  var response = await http.post( url+"/auth/register",
      headers: {"Content-type": "application/json"},body:json.encode(data));



  if(response.statusCode == 201) {
    return 201;
  }
  else if(response.statusCode == 400) {
    return 400;
  }
  else if(response.statusCode == 409) {
    return 409;
  }
  else {
    return 500;
  }
}
