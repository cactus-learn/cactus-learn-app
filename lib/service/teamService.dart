import 'dart:convert';

import 'package:cactus/config/config.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart'as http;
import 'package:cactus/config/config.dart';



Future getOrgaOfuserIn() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;
  try{
    response = await http.get( url + "/organisation/myOrganisations",
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      var r =  jsonDecode(response.body);
      return r.reversed.toList();
    }
    else{
      return null;
    }

  }

  else if(response.statusCode == 400 || response.statusCode == 401 ) {

  }
  else {
  }
}

