
import 'dart:convert';
import 'dart:developer';

import 'package:cactus/model/NoteModel.dart';
import 'package:cactus/utils/util.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart'as http;
import 'package:cactus/config/config.dart';

Future<List<NoteModel>> getUserNote() async{
  var response;
  final PATH = url +"/exercise/myResponses";

  String token = await getCurrentUserToken();
  try{
    response = await http.get(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    return null;
  }

  if(response.statusCode == 200) {


    if(response.body != "" && (json.decode(response.body) as List).isNotEmpty ){
//      log("DATA :"+response.body.toString());
      var data = (json.decode(response.body) as List)
          .map((data) => NoteModel.fromJsonData(data))
          .toList();
      return data;
    }
    else{
      return null;
    }
  }


}
