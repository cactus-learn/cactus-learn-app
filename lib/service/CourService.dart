import 'dart:convert';

import 'package:cactus/config/config.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart'as http;
import 'package:cactus/config/config.dart';

Future getCourOfOrga( String idOrga ) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;
  try{
    response = await http.get( url +"/organisation/all-groupe/" + idOrga,
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      var r =  jsonDecode(response.body);
      var group = r["groupes"] as List;
      return group.toList();
    }
    else{
      return null;
    }
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    //print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    //print(response.body);
  }

}


Future getLesson( String idLesson ) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;
  try{
    response = await http.get( url +"/lesson/" + idLesson,
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      var r =  jsonDecode(response.body);
      var lesson = r["chapters"] as List;
      return lesson.toList();
    }
    else{
      return null;
    }
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    //print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    //print(response.body);
  }

}
