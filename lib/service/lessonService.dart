

import 'dart:convert';

import 'package:cactus/model/LessonModel.dart';
import 'package:cactus/utils/util.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart'as http;
import 'package:cactus/config/config.dart';

Future<LessonModel> getLessonById(String lessonId) async {

  var response;
  final PATH = url +"/lesson/"+lessonId;

  String token = await getCurrentUserToken();
  try{
    response = await http.get(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      LessonModel data = LessonModel.fromJsonData(jsonDecode(response.body));
      return data;
    }
    else{
      return null;
    }
  }
}
