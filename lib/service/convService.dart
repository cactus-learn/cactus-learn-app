import 'dart:convert';
import 'dart:developer';

import 'package:cactus/config/config.dart';
import 'package:cactus/model/MessageModel.dart';
import 'package:cactus/model/UserDetails.dart';
import 'package:cactus/model/userResponse.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart'as http;


Future<dynamic>getUsersIdConversation() async{
  var response;

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
    response = await http.get( url +"/message/conversations",
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch (e) {
    print(e.toString());
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      var r =  jsonDecode(response.body);
      var group = r as List;
      return group.toList();
    }
    else{
      return null;
    }
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    //print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    //print(response.body);
  }
}

Future<UserResponse> getCurrentUserInfo() async{
  var response;

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
    response = await http.get( url +"/user/"+'info',
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch (e) {
    print(e.toString());
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      UserResponse r  =  UserResponse.fromJsonWithId(jsonDecode(response.body));
      return r;
    }
    else{
      return null;
    }
  }
  else{
    return null;
  }
}
Future<User>getUserById(String userId) async{
  var response;

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
    response = await http.get( url +"/user/"+userId,
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch (e) {
    print(e.toString());
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      User r  =  User.fromJsonData(jsonDecode(response.body));
      return r;
    }
    else{
      return null;
    }
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    //print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    //print(response.body);
  }


}

Future<List<MessageModel>> getMessageBetweenTwoUsers(String userId) async{
  var response;
  var result;
  final PATH = url +"/message/allMessagesBetweenMembers";
  Map<String, dynamic> data = {
    "receiverId" :userId,
  };
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
    var uri = Uri.parse(PATH);
    var request = Request('GET',uri);

    request.body = jsonEncode(data);
    request.headers.clear();
    request.headers.addAll({"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
    response = await request.send();
    result = await response.stream.bytesToString();
  }
  catch (e) {
    print(e.toString());
    return null;
  }
  if(response.statusCode == 200) {
    if(result != ""){
      var data = (json.decode(result) as List)
          .map((data) => MessageModel.fromJsonData(data))
          .toList();
      log("DATA Message TWO USERS:"+data.first.content.toString());
      return data;
    }
    else{
      return null;
    }
  }
}
Future<MessageModel> getMessageBetweenUser(String userId) async{
  var response;
  var result;
  final PATH = url +"/message/allMessagesBetweenMembers";
  Map<String, dynamic> data = {
    "receiverId" :userId,
  };
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
    var uri = Uri.parse(PATH);
    var request = Request('GET',uri);

    request.body = jsonEncode(data);
    request.headers.clear();
    request.headers.addAll({"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
    response = await request.send();
    result = await response.stream.bytesToString();
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(result != ""){
      MessageModel r;
      var data = (json.decode(result) as List)
          .map((data) => MessageModel.fromJsonData(data))
          .toList();
      r = data[0];
//      log("DATA Message :"+r.content);
      return r;
    }
    else{
      return null;
    }
  }


}
//TODO:
Future<String> getCurrentUserId() async{
  try {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.get("id");
  }catch(e){
    return "";

  }


}

Future<dynamic> sendMessage(String content, String receiverId) async{

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var jsonResponse = null;

  Map data = {
    'receiverId': receiverId,
    'content': content
  };
  var response;
  try {
    response = await http.post(url + "/message/send",
        headers:  {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")}, body: json.encode(data));
  }
  catch (e) {
    print(e.toString());
    return 500;
  }
  if(response.statusCode == 200) {
    jsonResponse = json.decode(response.body);
    if(jsonResponse != null) {
      return jsonResponse;
    }

  }else{
    return null;
  }
}

//void sendMessageToUser(String text, String receiverChatID) {
//  messages.add(Message(text, currentUser.chatID, receiverChatID));
//  socketIO.sendMessage(
//    'send_message',
//    json.encode({
//      'receiverChatID': receiverChatID,
//      'senderChatID': currentUser.chatID,
//      'content': text,
//    }),
//  );
//  notifyListeners();
//}
//
//List<Message> getMessagesForChatID(String chatID) {
//  return messages
//      .where((msg) => msg.senderID == chatID || msg.receiverID == chatID)
//      .toList();
//
//}
