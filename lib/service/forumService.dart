import 'dart:convert';
import 'dart:core';
import 'dart:developer';

import 'package:cactus/config/config.dart';
import 'package:cactus/model/ForumModel.dart';
import 'package:cactus/model/GroupModel.dart';
import 'package:cactus/model/OrganisationModel.dart';
import 'package:cactus/model/PostModel.dart';
import 'package:cactus/model/PostResponse.dart';
import 'package:cactus/utils/util.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart'as http;
import 'package:cactus/config/config.dart';

Future<List<OrganisationModel>> getUserOrganisations() async{
  var response;
  final PATH = url +"/organisation/myorganisations";

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
     response = await http.get(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      var data = (json.decode(response.body) as List)
          .map((data) => OrganisationModel.fromJsonData(data))
          .toList();
      //log("DATA Message :"+data[0].name);
      return data;
    }
    else{
      return null;
    }
  }


}


List<GroupModel> getGroupIdListFromOrganisation(List<OrganisationModel> list){
  List<GroupModel> result = [];
  for(OrganisationModel organisationModel in list){
    for(dynamic groupe in organisationModel.groupes){
      result.add(GroupModel.fromJsonId(groupe));
    }
  }
  return result;

}

Future<dynamic> ratePost(postId, bool value) async {
  var response;
  final PATH = url +"/organisation/groupe/forum/ratePost";
  Map data = {
    "postId": postId,
    "positif": value
  };

 String token = await getCurrentUserToken();

  try{
    response = await http.put(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token}, body: json.encode(data));
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      return true;
    }
    else{
      return false;
    }
  }
  else{
    return false;
  }
}


Future<PostResponse> createPost(String content,String forumId) async{

  var response;
  final PATH = url +"/organisation/groupe/forum/createPost";

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

  Map data = {
    'forumId': forumId,
    'content': content,
  };

  try{
    response = await http.post(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")}, body: json.encode(data));
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      PostResponse data = PostResponse.fromJsonData(jsonDecode(response.body));
      return data;
    }
    else{
      return null;
    }
  }

}

List<PostModel> getPostIdListFromForum(List<dynamic> list){
  List<PostModel> result = [];
  for(dynamic elem in list){
      result.add(PostModel.fromJsonId(elem));
  }
  return result;

}

Future<GroupModel> getUserGroup(String groupId) async{
  var response;
  final PATH = url +"/organisation/groupe/";

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
    response = await http.get(PATH+groupId, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
//      var data = (json.decode(response.body) as List)
//          .map((data) => GroupModel.fromJsonData(data))
//          .toList();
    GroupModel data = GroupModel.fromJsonData(jsonDecode(response.body));
      //log("DATA Message :"+data[0].name);
      return data;
    }
    else{
      return null;
    }
  }


}

Future<ForumModel> createForum(String forumName,String groupId, String organisationId)async{
  log("Data : "+ forumName + " "+ groupId+" "+ organisationId);
  var response;
  final PATH = url +"/organisation/groupe/forum/create";

 String token = await getCurrentUserToken();

  Map data = {
    'forumName': forumName,
    'groupeId': groupId,
    'organisationId': organisationId,
  };

  try{
    response = await http.post(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token}, body: json.encode(data));
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 201){
    if(response.body != ""){
      ForumModel data = ForumModel.fromJsonData(jsonDecode(response.body));
      return data;
    }
    else{
      return null;
    }
  }
}

Future<dynamic> deleteForum(String forumId) async{
  final PATH = url +"/organisation/groupe/forum/delete";
  var response;
  var result;
  String token = await getCurrentUserToken();
  String userId = await getCurrentUserId();

  Map data = {
    'forumId': forumId,
    'userId': userId,
  };
  try{
    var uri = Uri.parse(PATH);
    var request = http.Request('DELETE',uri);

    request.body = jsonEncode(data);
    request.headers.clear();
    request.headers.addAll({"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
    response = await request.send();
    result = await response.stream.bytesToString();  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(result != ""){
      var data = json.decode(result);
      return data;
    }
    else{
      return null;
    }
  }
}
Future<List<ForumModel>> getForumsFromGroup(String groupId) async{
  var response;
  final PATH = url +"/organisation/groupe/forum/getByGroup/"+groupId;

  String token = await getCurrentUserToken();
  try{
    response = await http.get(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      var data = (json.decode(response.body) as List)
          .map((data) => ForumModel.fromJsonData(data))
          .toList();
      return data;
    }
    else{
      return null;
    }
  }


}

Future<dynamic> openForum(String forumId) async{

  var response;
  final PATH = url +"/organisation/groupe/forum/open";

  String token = await getCurrentUserToken();
  Map data = {
    "forumId": forumId,
  };
  try{
    response = await http.put(PATH,body : jsonEncode(data),  headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){

      return true;
    }
    else{
      return false;
    }
  }

}

Future<dynamic> closeForum(String forumId) async{
  var response;
  final PATH = url +"/organisation/groupe/forum/close";

  String token = await getCurrentUserToken();

  Map data = {
    "forumId": forumId,
  };
  try{
    response = await http.put(PATH, body : jsonEncode(data), headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){

      return true;
    }
    else{
      return false;
    }
  }
}

Future<ForumModel> getForumById(String forumId) async{
  var response;
  final PATH = url +"/organisation/groupe/forum/";

  String token = await getCurrentUserToken();
  try{
    response = await http.get(PATH+forumId, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      ForumModel data = ForumModel.fromJsonDataWithPostModel(jsonDecode(response.body));
      return data;
    }
    else{
      return null;
    }
  }

}
Future<PostModel> getPostFromForum(String postId) async{
  var response;
  final PATH = url +"/organisation/groupe/forum/post/";

  String token = await getCurrentUserToken();  try{
    response = await http.get(PATH+postId, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){

      PostModel data = PostModel.fromJsonData(jsonDecode(response.body));
      return data;
    }
    else{
      return null;
    }
  }


}

