import 'dart:convert';
import 'dart:developer';

import 'package:cactus/config/config.dart';
import 'package:cactus/model/exerciceGlobal.dart';
import 'package:cactus/model/exercicesMdel.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart'as http;


Future<List<ExerciceGlobal>> getListExercices() async{
  var response;
  final PATH = url +"/exercise/myExercises";

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
    response = await http.get(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    print(response.body);
    if(response.body != ""){

      var data = (json.decode(response.body) as List)
          .map((data) => ExerciceGlobal.fromJsonData(data))
          .toList();
      //log("DATA Message :"+data[0].name);
      return data;
    }
    else{
      return null;
    }
  }

}


Future<ExerciceGlobal> getListExerciceByidGroup(String id) async{
  var response;
  final PATH = url +"/exercise/groupe/"+id;

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  try{
    response = await http.get(PATH, headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch (e) {
    print(e.toString());
    return null;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){
      ExerciceGlobal data = ExerciceGlobal.fromJsonDataExercice(jsonDecode(response.body));
      log("DATA Message :"+data.exercises.length.toString());

      return data;
    }
    else{
      return null;
    }
  }
}


Future<ExerciceGlobal> postUserResponse(ExercicesModel exercicesModel) async{
  var response;
  final PATH = url +"/exercise/send-response";

  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  Map data = {
    'exerciseId': exercicesModel.id,
    'questions': exercicesModel.questions
  };
  try{
    response = await http.post(PATH,
      headers:
        {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")},
      body: json.encode(data));
    return null;
  }
  catch (e) {
    print(e.toString());
    return null;
  }

}
