import 'dart:convert';

import 'package:cactus/config/config.dart';
import 'package:cactus/model/UserDetails.dart';
import 'package:cactus/model/userResponse.dart';
import 'package:cactus/utils/util.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart'as http;


Future getAllMemeberOfOrganisationIn() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;
  print(sharedPreferences.get("token"));
  try{
    response = await http.get( url + "/organisation/myOrganisations",
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      var r =  jsonDecode(response.body);
      return r.toList();
    }
    else{
      return null;
    }
  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    //print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    //print(response.body);
  }

}
Future<dynamic> updateUserFirstName(String newfirstName) async{

  var response;
  final PATH = url +"/user/updateFirstName";
  String userId = await getCurrentUserId();
  String token = await getCurrentUserToken();

  Map data = {
    "newfirstName": newfirstName,
    "userId": userId,
  };
  try{
    response = await http.put(PATH,body : jsonEncode(data),  headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    print(e.toString());
    return false;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){

      return true;
    }
    else{
      return false;
    }
  }
  else{
    return false;
  }


}
Future<dynamic> updateUserLastName(String newlastName) async{

  var response;
  final PATH = url +"/user/updateLastName";

  String token = await getCurrentUserToken();
  String userId = await getCurrentUserId();
  Map data = {
    "newLastName": newlastName,
    "userId": userId,
  };
  try{
    response = await http.put(PATH,body : jsonEncode(data),  headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch (e) {
    print(e.toString());
    return false;
  }

  if(response.statusCode == 200) {
    if(response.body != ""){

      return true;
    }
    else{
      return false;
    }
  }
  else{
    return false;
  }


}
Future<User> getUserInfo( String id ) async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  var response;

  try{
    response = await http.get( url +"/user/" + id,
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ sharedPreferences.get("token")});
  }
  catch( e ){
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      Map<String, dynamic> body = jsonDecode(response.body);
      User r =  User.fromJsonData(body);
        return r;
    }
    else{
      return null;
    }

  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    //print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    //print(response.body);
  }

}

Future<List<UserResponse>> getUserContacts() async {
  var response;
  String token = await getCurrentUserToken();
  try{
    response = await http.get( url +"/user/myContacts",
        headers: {"Content-type": "application/json", 'Authorization': 'Bearer '+ token});
  }
  catch( e ){
    return null;
  }
  if(response.statusCode == 200) {
    if(response.body != ""){
      List<UserResponse> data = (json.decode(response.body) as List)
          .map((data) => UserResponse.fromJsonPost(data))
          .toList();
      return data;
    }
    else{
      return null;
    }

  }
  else if(response.statusCode == 400 || response.statusCode == 401 ) {
    //print(res["message"]);
    //_showDialogErreur(res["message"]);
  }
  else {
    //print(response.body);
  }

}
